<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;

Route::middleware(['auth'])->group(function () {
    Route::get('/', [Controllers\DashboardController::class, 'index'])->name('dashboard');

    Route::prefix('/runners')->group(function () {
        Route::get('/', [Controllers\RunnerController::class, 'index'])->name('runners.index');

        Route::get('/{runner}/download', Controllers\RunnerDownloadController::class)->name('runners.download');
    });

    Route::prefix('/tracks')->group(function () {
        Route::get('/', [Controllers\TrackController::class, 'index'])->name('tracks.index');
        Route::get('/{track}', [Controllers\TrackController::class, 'show'])->name('tracks.show');
        Route::post('/{track}/start', Controllers\TrackStartController::class)->name('tracks.start');
    });

    Route::prefix('/settings')->group(function () {
        Route::get('/', [Controllers\SettingsController::class, 'index'])->name('settings.index');

        Route::get('/reports', [Controllers\Settings\ReportsController::class, 'index'])->name('reports.index');
        Route::get('/people', [Controllers\Settings\PeopleController::class, 'index'])->name('people.index');

        Route::get('/associations', [Controllers\Settings\AssociationsController::class, 'index'])->name('associations.index');
        Route::get('/users', [Controllers\Settings\UsersController::class, 'index'])->name('users.index');
        Route::get('/logs', [Controllers\Settings\LogsController::class, 'index'])->name('logs.index');

        Route::get('/editions', [Controllers\Settings\EditionsController::class, 'index'])->name('editions.index');
        Route::get('/editions/{edition}', [Controllers\Settings\EditionsController::class, 'show'])->name('editions.show');

        Route::get('/editions/{edition}/download', Controllers\Settings\EditionsDownloadController::class)->name('editions.download');
    });
});

require __DIR__ . '/auth.php';
