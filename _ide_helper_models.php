<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Association
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Runner[] $runners
 * @property-read int|null $runners_count
 * @method static \Database\Factories\AssociationFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Association newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Association newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Association query()
 * @method static \Illuminate\Database\Eloquent\Builder|Association whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Association whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Association whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Association whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Association whereUpdatedAt($value)
 */
	class Association extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Edition
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $opened_at
 * @property \Illuminate\Support\Carbon|null $closed_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $is_active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Runner[] $runners
 * @property-read int|null $runners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Track[] $tracks
 * @property-read int|null $tracks_count
 * @method static \Illuminate\Database\Eloquent\Builder|Edition currentEdition()
 * @method static \Database\Factories\EditionFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Edition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Edition query()
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereOpenedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Edition whereUpdatedAt($value)
 */
	class Edition extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Person
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property \Illuminate\Support\Carbon $birthdate
 * @property string $sex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Association[] $associations
 * @property-read int|null $associations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Runner[] $runners
 * @property-read int|null $runners_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Track[] $tracks
 * @property-read int|null $tracks_count
 * @method static \Database\Factories\PersonFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person query()
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereUpdatedAt($value)
 */
	class Person extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Runner
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $track_id
 * @property int $number
 * @property int $person_id
 * @property int|null $association_id
 * @property \Illuminate\Support\Carbon|null $started_at
 * @property \Illuminate\Support\Carbon|null $completed_at
 * @property-read \App\Models\Association|null $association
 * @property-read \App\Models\Person|null $person
 * @property-read \App\Models\Track|null $track
 * @method static \Database\Factories\RunnerFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Runner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Runner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereAssociationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner wherePersonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereTrackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Runner whereUpdatedAt($value)
 */
	class Runner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Track
 *
 * @property int $id
 * @property string $name
 * @property int $edition_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Edition|null $edition
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Runner[] $runners
 * @property-read int|null $runners_count
 * @method static \Database\Factories\TrackFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Track newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Track newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Track query()
 * @method static \Illuminate\Database\Eloquent\Builder|Track whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Track whereEditionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Track whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Track whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Track whereUpdatedAt($value)
 */
	class Track extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

