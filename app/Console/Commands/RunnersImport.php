<?php

namespace App\Console\Commands;

use App\Actions\Api\Editions\CurrentEdition;
use App\Models\Association;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class RunnersImport extends Command
{
    protected $signature = 'runners:import {file : path to import file}';
    protected $description = 'Import runners using a CSV file';

    public function handle()
    {
        $file = $this->argument('file');
        if (!file_exists($file)) {
            $this->error('The provided file does not exist.');
            return static::INVALID;
        }

        $path = realpath($file);
        $this->info("Importing data using {$path}...");

        $csv = file_get_contents($file);
        $lines = explode(PHP_EOL, $csv);

        $errors = [];
        $currentLine = 0;
        $this->withProgressBar($lines, function ($runner) use (&$errors, &$currentLine) {
            if (!$runner) return;

            $data = explode(',', $runner);
            $lastName = $data[0] ?? '';
            $firstName = $data[1] ?? '';
            $associationName = $data[2] ?? '';
            $trackName = $data[3] ?? '';

            $person = Person::create([
                'last_name' => ucwords(Str::of($lastName)->lower()),
                'first_name' => ucwords(Str::of($firstName)->lower())
            ]);
            $track = Track::whereName($trackName)->firstOrNew(['name' => $trackName]);

            if (!$track->edition_id)
                $track->edition()->associate(
                    app()->make(CurrentEdition::class)->find()
                )->save();

            $association = $associationName
                ? Association::whereName($associationName)
                    ->orWhere('slug', Str::slug($associationName))
                    ->firstOrCreate(['name' => $associationName])
                : new Association;

            $runner = new Runner;
            $runner->person()->associate($person);
            $runner->track()->associate($track);
            $runner->association()->associate($association);
            $runner->number = $track->runners()->count() + 1;

            try {
            $runner->save();
            } catch (Exception $error) {
                array_push($errors, "Could not save runner: {$person->last_name} {$person->first_name}");
            } finally {
                $currentLine += 1;
            }
        });

        if (count($errors)) {
            foreach ($errors as $error) $this->warn($error);
            return static::FAILURE;
        }

        return static::SUCCESS;
    }
}
