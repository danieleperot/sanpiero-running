<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Person extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = ['first_name', 'last_name', 'birthdate', 'sex'];
    protected $dates = ['birthdate'];

    public function runners(): HasMany
    {
        return $this->hasMany(Runner::class);
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logAll()
            ->logExcept(['updated_at', 'created_at'])
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    public function tracks(): HasManyThrough
    {
        return $this->hasManyThrough(
            Track::class,
            Runner::class,
            'person_id',
            'id',
            'id',
            'track_id'
        );
    }

    public function associations(): HasManyThrough
    {
        return $this->hasManyThrough(
            Association::class,
            Runner::class,
            'person_id',
            'id',
            'id',
            'association_id'
        );
    }

    public function sexLabel(): Attribute
    {
        return Attribute::get(function () {
           if ($this->sex === 'f') return 'Donna';
            if ($this->sex === 'm') return 'Uomo';

            return '-';
        });
    }
}
