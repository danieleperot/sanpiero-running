<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Edition extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'name', 'opened_at', 'closed_at', 'is_active', 'require_only_name'
    ];
    protected $dates = ['opened_at', 'closed_at'];
    protected $casts = [
        'is_active' => 'boolean',
        'require_only_name' => 'boolean'
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logAll()
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    public function tracks(): HasMany
    {
        return $this->hasMany(Track::class);
    }

    public function runners(): HasManyThrough
    {
        return $this->hasManyThrough(Runner::class, Track::class);
    }

    public function scopeCurrentEdition(Builder $query): Builder
    {
        return $query->whereName('test');
    }
}
