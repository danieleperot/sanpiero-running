<?php
namespace App\Exports;

use App\Exports\Sheets\EditionRunnersSheet;
use App\Exports\Sheets\EditionTrackSheet;
use App\Models\Edition;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class EditionExport implements WithMultipleSheets
{
    use Exportable;

    public function __construct(private Edition $edition) {}

    public function sheets(): array
    {
        return $this->edition
            ->tracks
            ->map(fn ($track) => new EditionTrackSheet($track))
            ->push(new EditionRunnersSheet($this->edition))
            ->toArray();
    }
}
