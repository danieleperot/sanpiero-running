<?php

namespace App\Exports\Sheets;

use App\Models\Runner;
use App\Models\Track;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class EditionTrackSheet implements FromCollection, WithTitle, WithHeadings
{
    public function __construct(private Track $track) {}

    public function title(): string
    {
        return 'Percorso ' . $this->track->name;
    }

    public function collection()
    {
        return $this->track->runners()->with('person', 'association')->get()->map(function (Runner $runner) {
            return [
                'number' => $runner->number,
                'last_name' => $runner->person->last_name,
                'first_name' => $runner->person->first_name,
                'sex' => $runner->person->sex_label,
                'birthyear' => optional($runner->person->birthdate)->format('Y'),
                'association' => optional($runner->association)->name,
                'family' => $runner->family,
                'started_at' => $runner->started_at,
                'completed_at' => $runner->completed_at,
                'total_seconds' => optional($runner->completed_at)->diffInRealSeconds($runner->started_at),
                'total_nice' => optional(optional($runner->completed_at)->diff($runner->started_at))->format('%Hh %Im %Ss')
            ];
        });
    }

    public function headings(): array
    {
        return [
            'Pettorale',
            'Cognome',
            'Nome',
            'Sesso',
            'Anno di nascita',
            'Associazione',
            'Famiglia',
            'Data e ora di partenza',
            'Data e ora di arrivo',
            'Tempo totale in secondi',
            'Tempo totale per umani',
        ];
    }
}
