<?php

namespace App\Enums;

enum UserRole: string
{
    case OPERATOR = 'operator';
    case TIMEKEEPER = 'timekeeper';
    case SUPERVISOR = 'supervisor';
    case ADMIN = 'admin';

    public function label(): string
    {
        return match ($this->value) {
            'operator' => 'Operatore',
            'timekeeper' => 'Cronometrista',
            'supervisor' => 'Supervisore',
            'admin' => 'Amministratore',
            default => 'Ruolo non riconosciuto'
        };
    }

    public function description(): string
    {
        return match ($this->value) {
            'operator' => "Può inserire e modificare (prima della partenza) i partecipanti alle gare dell'edizione.",
            'timekeeper' => "Può far cominciare le gare dell'edizione e registrare i tempi dei partecipanti.",
            'supervisor' => "Può svolgere il ruolo di operatore e cronometrista, oltre a gestire i percorsi e accedere ai log dell'edizione in corso.",
            'admin' => "Gestisce tutti gli aspetti della San Piero Running. Oltre a svolgere il ruolo di supervisore, può creare edizioni e nuovi utenti.",
            default => 'Questo ruolo non è riconosciuto, e pertanto non può fare nulla.'
        };
    }

}
