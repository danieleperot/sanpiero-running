<?php

namespace App\Providers;

use App\Actions\Api;
use App\Actions\Eloquent;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->app->bind(Api\Editions\CurrentEdition::class, Eloquent\Editions\CurrentEdition::class);

        $this->app->bind(Api\Runners\RunnersList::class, Eloquent\Runners\RunnersList::class);
        $this->app->bind(Api\Runners\RunnerCreator::class, Eloquent\Runners\RunnerCreator::class);

        $this->app->bind(Api\Tracks\ActiveTracks::class, Eloquent\Tracks\ActiveTracks::class);
    }
}
