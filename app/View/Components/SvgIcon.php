<?php

namespace App\View\Components;

use DOMDocument;
use Illuminate\View\Component;

class SvgIcon extends Component
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function icon(): string
    {
        $path = resource_path("svg/{$this->name}.svg");

        if (!file_exists($path)) return '';

        $svg = new DOMDocument();
        $svg->load($path);

        foreach ($this->attributes as $name => $attribute)
            $svg->documentElement->setAttribute($name, $attribute);

        return $svg->saveXML($svg->documentElement);
    }

    public function render(): string
    {
        return <<<'blade'
            @if ($icon)
            {!! $icon !!}
            @else
            <div></div>
            @endif
        blade;
    }
}
