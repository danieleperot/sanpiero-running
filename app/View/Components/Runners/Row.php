<?php

namespace App\View\Components\Runners;

use App\Models\Runner;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Row extends Component
{
    private Runner $runner;

    public function __construct(Runner $runner)
    {
        $this->runner = $runner;
    }

    public function number(): string
    {
        return sprintf('#%d', $this->runner->number);
    }

    public function id()
    {
        return $this->runner->id;
    }

    public function firstName(): string
    {
        return $this->runner->person->first_name ?? '';
    }

    public function lastName(): string
    {
        return $this->runner->person->last_name ?? '';
    }

    public function track(): string
    {
        return $this->runner->track->name ?? '';
    }

    public function sex(): string
    {
        $sex = $this->runner->person->sex ?? '';

        return ['m' => 'Uomo', 'f' => 'Donna'][$sex] ?? '-';
    }

    public function birthdate(): string
    {
        $birthdate = $this->runner->person->birthdate ?? '';

        return $birthdate ? Carbon::parse($birthdate)->format('Y') : '-';
    }

    public function age(): string
    {
        $birthdate = optional(optional($this->runner)->person)->birthdate ?: '';

        return $birthdate
            ? sprintf('%d anni', Carbon::parse($birthdate)->diffInYears(now()))
            : '';
    }

    public function associationName(): string
    {
        $association = $this->runner->association->name ?? '';

        return $association ?: 'Senza associazione';
    }

    public function family(): string
    {
        return $this->runner->family ?: 'Non specificata';
    }

    public function confirmDownloadName(): string
    {
        return Str::slug(
            $this->lastName()
            . ' ' . $this->firstName()
            . ' ' . 'conferma_iscrizione.pdf'
        );
    }

    public function render()
    {
        return view('components.runners.row');
    }
}
