<?php

namespace App\View\Components;

use Illuminate\View\Component;

class AppLayout extends Component
{
    /** @var string */
    public $title;

    public function __construct(String $title = '')
    {
        $this->title = $title;
    }

    public function render()
    {
        return view('layouts.app');
    }
}
