<?php

namespace App\Actions\Dto\Runners;

use Spatie\LaravelData\Data;

class SaveNewPayload extends Data
{
    public function __construct(
        public string $firstName = '',
        public string $lastName = '',
        public ?string $birthdate = null,
        public ?string $sex = null,
        public string $number = '',
        public string $trackId = '',
        public string $association = '',
        public string $family = ''
    ) {}
}
