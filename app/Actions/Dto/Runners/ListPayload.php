<?php

namespace App\Actions\Dto\Runners;

use Spatie\LaravelData\Data;

class ListPayload extends Data
{
    const CURRENT_EDITON = 'current';
    const ALL_EDITIONS = 'all';

    public function __construct(
        public int $page = 1,
        public string $editions = self::ALL_EDITIONS,
        public ?int $trackId = null,
        public string $search = ''
    ) {}
}
