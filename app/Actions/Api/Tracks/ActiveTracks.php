<?php

namespace App\Actions\Api\Tracks;

use App\Models\Track;
use Illuminate\Support\Collection;

interface ActiveTracks
{
    /** @return Collection<Track> */
    public function find(): Collection;
}
