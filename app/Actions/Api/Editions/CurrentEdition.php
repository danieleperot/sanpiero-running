<?php

namespace App\Actions\Api\Editions;

use App\Models\Edition;

interface CurrentEdition
{
    public function find(): Edition;
}
