<?php

namespace App\Actions\Api\Runners;

use App\Actions\Dto\Runners\SaveNewPayload;
use App\Models\Runner;

interface RunnerCreator
{
    public function create(SaveNewPayload $payload);

    public function update(Runner $runner, SaveNewPayload $payload);
}
