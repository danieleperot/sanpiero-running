<?php

namespace App\Actions\Api\Runners;

use App\Actions\Dto\Runners\ListPayload;
use Illuminate\Pagination\LengthAwarePaginator;

interface RunnersList
{
    public function get(ListPayload $payload): LengthAwarePaginator;
}
