<?php

namespace App\Actions\Eloquent\Editions;

use App\Actions\Api\Editions\CurrentEdition as CurrentEditionInterface;
use App\Models\Edition;
use Illuminate\Database\Eloquent\Builder;

class CurrentEdition implements CurrentEditionInterface
{
    public function find(): Edition
    {
        $activeEdition = Edition::whereIsActive(true)->first();
        if ($activeEdition) return $activeEdition;

        return Edition::where(
            fn(Builder $query) => $query
                ->where('closed_at', '>', now())
                ->orWhereNull('closed_at')
        )->where(
            fn(Builder $query) => $query
                ->where('opened_at', '<', now())
                ->orWhereNull('opened_at')
        )->firstOrNew();
    }
}
