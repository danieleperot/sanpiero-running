<?php

namespace App\Actions\Eloquent\Tracks;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Tracks\ActiveTracks as ActiveTracksInterface;
use App\Models\Track;
use Illuminate\Support\Collection;

class ActiveTracks implements ActiveTracksInterface
{
    private CurrentEdition $currentEdition;

    public function __construct(CurrentEdition $currentEdition)
    {
        $this->currentEdition = $currentEdition;
    }

    public function find(): Collection
    {
        $currentEdition = $this->currentEdition->find()->id;

        return Track::with('edition')
            ->withCount('runners')
            ->whereEditionId($currentEdition)
            ->get();
    }
}
