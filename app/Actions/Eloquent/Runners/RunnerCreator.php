<?php

namespace App\Actions\Eloquent\Runners;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Runners\RunnerCreator as RunnerCreatorInterface;
use App\Actions\Dto\Runners\SaveNewPayload;
use App\Models\Association;
use App\Models\Edition;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\RequiredIf;

class RunnerCreator implements RunnerCreatorInterface
{
    private Edition $edition;

    public function __construct()
    {
        $this->edition = app()->make(CurrentEdition::class)->find();
    }

    public function create(SaveNewPayload $payload)
    {
        $this->validatePayload($payload);

        $runner = new Runner;
        if ($payload->family) $runner->family = $payload->family;

        $runner->number = $payload->number
            ?: Runner::count() + 1;

        $runner->track()->associate($payload->trackId);
        $runner->person()->associate(Person::create([
            'first_name' => $payload->firstName,
            'last_name' => $payload->lastName,
            'birthdate' => $payload->birthdate,
            'sex' => $payload->sex
        ])->id);

        if ($payload->association)
            $runner->association()->associate($this->findAssociation($payload));

        $runner->save();
    }

    public function update(Runner $runner, SaveNewPayload $payload)
    {
        $this->validatePayload($payload, $runner);

        $runner->number = $payload->number;
        if ($payload->family) $runner->family = $payload->family;

        $runner->track()->associate($payload->trackId);

        $runner->person->update([
            'first_name' => $payload->firstName,
            'last_name' => $payload->lastName,
            'birthdate' => $payload->birthdate,
            'sex' => $payload->sex
        ]);

        if ($payload->association)
            $runner->association()->associate($this->findAssociation($payload));

        $runner->save();
    }

    private function validatePayload(SaveNewPayload $payload, ?Runner $runner = null)
    {
        Validator::make($payload->toArray(), [
            'firstName' => ['required'],
            'lastName' => ['required'],
            'birthdate' => [new RequiredIf(fn () => !$this->edition->require_only_name)],
            'sex' => [new RequiredIf(fn () => !$this->edition->require_only_name)],
            'trackId' => ['required', new Exists('tracks', 'id')],
            'number' => [new RequiredIf(fn () => !$this->edition->require_only_name), $this->validateNumber($payload, $runner)]
        ])->validate();
    }

    private function findAssociation(SaveNewPayload $payload): Association
    {
        $association = Association::make(['name' => $payload->association]);
        $storedAssociation = Association::whereSlug($association->slug)->first();

        if ($storedAssociation) return $storedAssociation;
        $association->save();

        return $association;
    }

    private function validateNumber(SaveNewPayload $payload, ?Runner $runner = null): callable
    {
        return function ($attribute, $value, $fail) use ($payload, $runner) {
            if (!$value) return;

            $message = 'Questo pettorale è già in uso per il percorso specificato.';
            $query = Runner::whereNumber($value)->whereTrackId($payload->trackId);

            if ($runner) $query->where('id', '<>', $runner->id);

            if ($query->count()) $fail($message);
        };
    }
}
