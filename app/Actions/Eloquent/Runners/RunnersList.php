<?php

namespace App\Actions\Eloquent\Runners;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Runners\RunnersList as RunnersListInterface;
use App\Actions\Dto\Runners\ListPayload;
use App\Models\Runner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class RunnersList implements RunnersListInterface
{
    public function __construct(private CurrentEdition $currentEdition) {}

    public function get(ListPayload $payload): LengthAwarePaginator
    {
        $query = Runner::with('person', 'association')->orderByDesc('created_at');

        if ($payload->editions === $payload::CURRENT_EDITON)
            $query->whereHas('track', $this->filterCurrentEdition());

        if ($payload->trackId)
            $query->whereTrackId($payload->trackId)->reorder()
                ->orderByRaw('CASE WHEN completed_at IS NULL THEN 1 ELSE 0 END')
                ->orderBy('completed_at');

        if ($payload->search) $this->filterSearch($query, $payload->search);

        return $query->paginate(12, ['*'], 'page', $payload->page);
    }

    private function filterCurrentEdition(): callable
    {
        return fn(Builder $builder) => $builder->where(
            'edition_id',
            $this->currentEdition->find()->id
        );
    }

    private function filterSearch(Builder &$query, string $search)
    {
        $query->whereHas('person', function (Builder $inner) use ($search) {
            $inner->where('first_name', 'LIKE', "%{$search}%")
                ->orWhere('last_name', 'LIKE', "%{$search}%");
        });
    }
}
