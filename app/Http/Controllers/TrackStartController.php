<?php

namespace App\Http\Controllers;

use App\Models\Runner;
use App\Models\Track;

class TrackStartController
{
    public function __invoke(Track $track)
    {
        Runner::query()->update(['started_at' => now()]);

        return response()->redirectToRoute('tracks.show', $track);
    }
}
