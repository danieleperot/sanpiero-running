<?php

namespace App\Http\Controllers;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Tracks\ActiveTracks;
use App\Models\Track;

class TrackController extends Controller
{
    public function __construct(private ActiveTracks $activeTracks) {}

    public function index(CurrentEdition $currentEdition)
    {
        $edition = $currentEdition->find();

        return view('tracks.index', [
            'tracks' => $this->activeTracks->find(),
            'edition' => $edition
        ]);
    }

    public function show(Track $track)
    {
        return view('tracks.show', ['track' => $track]);
    }
}
