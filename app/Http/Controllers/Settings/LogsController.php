<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;

class LogsController extends Controller
{
    public function index()
    {
        return view('settings.logs');
    }
}
