<?php

namespace App\Http\Controllers\Settings;

use App\Exports\EditionExport;
use App\Http\Controllers\Controller;
use App\Models\Edition;
use Illuminate\Support\Str;

class EditionsDownloadController extends Controller
{
    public function __invoke(Edition $edition)
    {
        $fileName = Str::of($edition->name)->slug() . '.xlsx';

        return (new EditionExport($edition))->download($fileName);
    }
}
