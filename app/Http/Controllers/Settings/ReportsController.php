<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function index()
    {
        return view('settings.reports');
    }
}
