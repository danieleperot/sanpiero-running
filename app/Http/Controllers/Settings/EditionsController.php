<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Models\Edition;

class EditionsController extends Controller
{
    public function index()
    {
        return view('settings.editions.index');
    }

    public function show(Edition $edition)
    {
        return view('settings.editions.show', [
            'edition' => $edition,
            'tracks' => $edition->tracks->loadCount('runners'),
            'columns' => [
                ['label' => __('ID'), 'traverse' => 'id'],
                ['label' => __('Nome percorso'), 'traverse' => 'name'],
                ['label' => __('Partecipanti'), 'traverse' => 'runners_count']
            ]
        ]);
    }
}
