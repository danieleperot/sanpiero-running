<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
        return view('settings.users');
    }
}
