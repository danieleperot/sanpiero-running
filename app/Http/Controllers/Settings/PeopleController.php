<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;

class PeopleController extends Controller
{
    public function index()
    {
        return view('settings.people');
    }
}
