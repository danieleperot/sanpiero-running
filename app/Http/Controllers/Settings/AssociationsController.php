<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;

class AssociationsController extends Controller
{
    public function index()
    {
        return view('settings.associations');
    }
}
