<?php

namespace App\Http\Controllers;

class SettingsController extends Controller
{
    public function index()
    {
        return redirect()->route('editions.index');
    }
}
