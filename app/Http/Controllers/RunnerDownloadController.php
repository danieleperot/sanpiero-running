<?php

namespace App\Http\Controllers;

use App\Models\Runner;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Str;

class RunnerDownloadController extends Controller
{
    public function __invoke(Runner $runner)
    {
        $runner = $runner->load('person', 'track', 'track.edition', 'association');
        $fileName = Str::slug($runner->track->name) . '_' . $runner->number . '.pdf';

        return Pdf::loadView('pdf.runners.confirmation', [
            'runner' => $runner
        ])->download($fileName);
    }
}
