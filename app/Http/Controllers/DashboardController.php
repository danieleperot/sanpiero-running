<?php

namespace App\Http\Controllers;

use App\Actions\Api\Editions\CurrentEdition;
use Illuminate\Database\Eloquent\Builder;

class DashboardController extends Controller
{
    public function index(CurrentEdition $currentEdition)
    {
        $edition = $currentEdition->find();

        return view('dashboard', [
            'edition' => $edition,
            'expectedAmountPaid' => $edition
                ->runners()
                ->whereHas('person', fn (Builder $builder) => $builder
                    ->whereDate('birthdate', '<=', now()->subYears(7))
                )->count() * config('sanpiero.tickets.price')
        ]);
    }
}
