<?php

namespace App\Http\Controllers;

use App\Actions\Api\Editions\CurrentEdition;

class RunnerController extends Controller
{
    public function index(CurrentEdition $currentEdition)
    {
        $edition = $currentEdition->find();

        return view('runners.index', ['edition' => $edition]);
    }
}
