<?php

namespace App\Http\Livewire\Associations;

use App\Models\Association;
use LivewireUI\Modal\ModalComponent;

class Create extends ModalComponent
{
    public array $association = [];

    protected $rules = ['association.name' => 'required'];

    public function submit()
    {
        $this->validate();

        Association::create($this->association);

        return redirect()->route('associations.index');
    }

    public function render()
    {
        return view('livewire.associations.create');
    }
}
