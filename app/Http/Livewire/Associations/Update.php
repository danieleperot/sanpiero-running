<?php

namespace App\Http\Livewire\Associations;

use App\Models\Association;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $associationId = 0;
    public array $association = [];

    protected $rules = ['association.name' => 'required'];

    public function mount(int $associationId = 0)
    {
        $this->associationId = $associationId;

        $association = Association::find($associationId);
        $this->association = $association->toArray();
    }

    public function submit()
    {
        $this->validate();

        $association = Association::find($this->associationId);
        $association->update($this->association);

        return redirect()->route('associations.index');
    }

    public function render()
    {
        return view('livewire.associations.update');
    }
}
