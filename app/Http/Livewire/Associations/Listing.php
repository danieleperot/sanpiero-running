<?php

namespace App\Http\Livewire\Associations;

use App\Models\Association;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class Listing extends Component
{
    public int $page = 1;

    protected $queryString = ['page'];

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('ID'), 'traverse' => 'id'],
            ['label' => __('Nome associazione'), 'traverse' => 'name'],
            ['label' => __('Iscritti'), 'traverse' => 'runners_count']
        ];
    }

    public function associations(): LengthAwarePaginator
    {
        return Association::withCount(['runners'])
            ->orderBy('name')
            ->paginate(12, ['*'], 'page', $this->page);
    }

    public function render()
    {
        return view('livewire.associations.listing');
    }
}
