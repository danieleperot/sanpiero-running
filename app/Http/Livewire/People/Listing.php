<?php

namespace App\Http\Livewire\People;

use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class Listing extends Component
{
    public int $page = 1;

    protected $queryString = ['page'];

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('ID'), 'traverse' => 'id'],
            ['label' => __('Cognome'), 'traverse' => 'last_name'],
            ['label' => __('Nome'), 'traverse' => 'first_name'],
            [
                'label' => __('Anno di nascita'),
                'traverse' => 'birthdate',
                'filter' => function ($value, $person) {
                    if (!$value) return;
                    
                    return $person->birthdate->format('Y')
                        . " ({$person->birthdate->diffInYears(now())} anni)";
                }
            ],
            ['label' => __('Sesso'), 'traverse' => 'sex_label'],
            ['label' => __('Gare partecipate'), 'traverse' => 'runners_count'],
        ];
    }

    public function people(): LengthAwarePaginator
    {
        return Person::withCount('runners')
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->paginate(12, ['*'], 'page', $this->page);
    }

    public function render()
    {
        return view('livewire.people.listing');
    }
}
