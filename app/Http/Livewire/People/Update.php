<?php

namespace App\Http\Livewire\People;

use App\Models\Person;
use Carbon\Carbon;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $personId = 0;
    public array $person = [];

    protected $rules = [
        'person.last_name' => 'required',
        'person.first_name' => 'required',
        'person.birthdate' => 'required',
        'person.sex' => 'required',
    ];

    public function mount(int $personId = 0)
    {
        $this->personId = $personId;

        $person = Person::find($personId);
        $this->person = $person->toArray();
        $this->person['birthdate'] = $person->birthdate->format('Y');
    }

    public function submit()
    {
        $this->validate();

        $person = Person::find($this->personId);
        $this->person['birthdate'] = Carbon::now()->setDay(1)->setMonth(1)->setYear($this->person['birthdate']);
        $person->update($this->person);

        return redirect()->route('people.index');
    }

    public function render()
    {
        return view('livewire.people.update');
    }
}
