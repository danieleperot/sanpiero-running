<?php

namespace App\Http\Livewire\Users;

use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class Listing extends Component
{
    public int $page = 1;

    protected $queryString = ['page'];

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('ID'), 'traverse' => 'id'],
            ['label' => __('Nome e cognome'), 'traverse' => 'name'],
            ['label' => __('Indirizzo email'), 'traverse' => 'email'],
            ['label' => __('Ruolo'), 'traverse' => 'role', 'filter' => fn ($role, $user) => optional($user->role)->label()],
        ];
    }

    public function users(): LengthAwarePaginator
    {
        return User::query()
            ->orderBy('name')
            ->paginate(12, ['*'], 'page', $this->page);
    }

    public function render()
    {
        return view('livewire.users.listing');
    }
}
