<?php

namespace App\Http\Livewire\Users;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Unique;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $userId = 0;
    public array $user = [];

    public function rules()
    {
        return [
            'user.name' => 'required',
            'user.email' => ['required', 'email'],
            'user.password' => [
                'confirmed',
                (new Unique('users', 'email'))->ignore($this->userId)
            ],
            'user.role' => ['required']
        ];
    }

    public function mount(int $userId = 0)
    {
        $this->userId = $userId;

        $user = User::find($userId);
        $this->user = $user->toArray();
    }

    public function submit()
    {
        $this->validate();

        $user = User::find($this->userId);
        $update = [
            'name' => $this->user['name'],
            'email' => $this->user['email'],
            'role' => $this->user['role'],
        ];
        if ($this->user['password'] ?? false)
            $update['password'] = Hash::make($this->user['password']);

        $user->update($update);

        return redirect()->route('users.index');
    }

    public function render()
    {
        return view('livewire.users.update');
    }
}
