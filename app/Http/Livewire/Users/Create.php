<?php

namespace App\Http\Livewire\Users;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use LivewireUI\Modal\ModalComponent;

class Create extends ModalComponent
{
    public array $user = [];

    protected $rules = [
        'user.name' => 'required',
        'user.email' => 'required|email|unique:users,email',
        'user.password' => 'required|confirmed',
        'user.role' => 'required'
    ];

    public function submit()
    {
        $this->validate();

        User::create([
            'name' => $this->user['name'],
            'email' => $this->user['email'],
            'role' => $this->user['role'],
            'password' => Hash::make($this->user['password'])
        ]);

        return redirect()->route('users.index');
    }

    public function render()
    {
        return view('livewire.users.create');
    }
}
