<?php

namespace App\Http\Livewire\Logs;

use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;
use Spatie\Activitylog\Models\Activity;

class Listing extends Component
{
    public int $page = 1;

    protected $queryString = ['page'];

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('Data'), 'traverse' => 'created_at', 'filter' => fn ($date) => $date->format('d/m/Y H:i:s')],
            ['label' => __('Eseguito da'), 'traverse' => 'causer_id', 'filter' => function ($id, $log) {
                if (!$id) return 'Non disponibile';

                return "{$log->causer->name}<br />({$log->causer->email})";
            }],
            ['label' => __('Descrizione'), 'traverse' => 'description'],
            ['label' => __('Soggetto'), 'traverse' => 'subject_type', 'filter' => fn ($type, $log) => "{$type}::{$log->subject_id}"],
            ['label' => __('Dati precedenti'), 'traverse' => 'properties.old', 'as' => 'json'],
            ['label' => __('Dati aggiornati'), 'traverse' => 'properties.attributes', 'as' => 'json'],
        ];
    }

    public function logs(): LengthAwarePaginator
    {
        return Activity::with('causer')
            ->orderByDesc('created_at')
            ->paginate(12, ['*'], 'page', $this->page);
    }

    public function render()
    {
        return view('livewire.logs.listing');
    }
}
