<?php

namespace App\Http\Livewire\Editions;

use App\Models\Edition;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $editionId = 0;
    public array $edition = [];

    protected $rules = ['edition.name' => 'required'];

    public function mount(int $editionId = 0)
    {
        $this->editionId = $editionId;

        $edition = Edition::find($editionId);
        $this->edition = $edition->toArray();
        $this->edition['opened_at'] = optional($edition->opened_at)->format('Y-m-d');
        $this->edition['closed_at'] = optional($edition->closed_at)->format('Y-m-d');
    }

    public function submit()
    {
        $this->validate();

        if ($this->edition['is_active'] ?? false)
            Edition::whereIsActive(true)->update(['is_active' => false]);

        $edition = Edition::find($this->editionId);
        $edition->update(
            collect($this->edition)
                ->mapWithKeys(fn ($value, $key) => [$key => $value ?: null])
                ->put('require_only_name', $this->edition['require_only_name'] ?? false)
                ->put('is_active', $this->edition['is_active'] ?? false)
                ->toArray()
        );

        return redirect()->route('editions.show', $edition);
    }

    public function render()
    {
        return view('livewire.editions.update');
    }
}
