<?php

namespace App\Http\Livewire\Editions;

use App\Models\Edition;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class Listing extends Component
{
    public int $page = 1;

    protected $queryString = ['page'];

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('ID'), 'traverse' => 'id'],
            ['label' => __('Nome edizione'), 'traverse' => 'name'],
            [
                'label' => __('In corso'),
                'traverse' => 'is_active',
                'filter' => fn ($value) => $value ? 'Sì' : 'No'
            ],
            ['label' => __('Percorsi'), 'traverse' => 'tracks_count'],
            ['label' => __('Partecipanti'), 'traverse' => 'runners_count']
        ];
    }

    public function editions(): LengthAwarePaginator
    {
        return Edition::withCount(['tracks', 'runners'])
            ->orderByDesc('created_at')
            ->orderBy('name')
            ->paginate(12, ['*'], 'page', $this->page);
    }

    public function render()
    {
        return view('livewire.editions.listing');
    }
}
