<?php

namespace App\Http\Livewire\Editions;

use App\Models\Edition;
use LivewireUI\Modal\ModalComponent;

class CreateForm extends ModalComponent
{
    public array $edition = [];

    protected $rules = [
        'edition.name' => 'required|unique:editions,name',
    ];

    public function submit()
    {
        $this->validate();

        if ($this->edition['is_active'] ?? false)
            Edition::whereIsActive(true)->update(['is_active' => false]);

        $edition = Edition::create($this->edition);

        return redirect()->route('editions.show', $edition);
    }

    public function render()
    {
        return view('livewire.editions.create-form');
    }
}
