<?php

namespace App\Http\Livewire\Editions\Tracks;

use App\Models\Track;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $trackId = 0;
    public array $track = [];

    protected $rules = ['track.name' => 'required'];

    public function mount(int $trackId = 0)
    {
        $this->trackId = $trackId;

        $track = Track::find($trackId);
        $this->track = $track->toArray();
    }

    public function submit()
    {
        $this->validate();

        $track = Track::find($this->trackId);
        $track->update($this->track);

        return redirect()->route('editions.show', $track->edition);
    }

    public function render()
    {
        return view('livewire.editions.tracks.update');
    }
}
