<?php

namespace App\Http\Livewire\Editions\Tracks;

use App\Models\Edition;
use LivewireUI\Modal\ModalComponent;

class Create extends ModalComponent
{
    public int $editionId = 0;
    public array $track = [];

    protected $rules = ['track.name' => 'required'];

    public function submit()
    {
        $this->validate();

        $edition = Edition::find($this->editionId);
        $edition->tracks()->create($this->track);

        return redirect()->route('editions.show', $edition);
    }

    public function render()
    {
        return view('livewire.editions.tracks.create');
    }
}
