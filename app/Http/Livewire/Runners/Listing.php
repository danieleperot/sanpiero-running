<?php

namespace App\Http\Livewire\Runners;

use App\Actions\Api\Runners\RunnersList;
use App\Actions\Dto\Runners\ListPayload;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class Listing extends Component
{
    public int $page = 1;
    public ?int $trackId = null;
    public ?int $editionId = null;
    public string $search = '';

    protected $queryString = ['page'];

    protected $listeners = [FastForm::SAVED_EVENT => '$refresh'];

    public function mount(?int $trackId = null)
    {
        $this->trackId = $trackId;
    }

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function runners(): LengthAwarePaginator
    {
        $request = new ListPayload($this->page, ListPayload::CURRENT_EDITON);
        if ($this->trackId) $request->trackId = $this->trackId;
        if ($this->search) $request->search = $this->search;

        $runnersList = app()->make(RunnersList::class);

        return $runnersList->get($request);
    }

    public function render()
    {
        return view('livewire.runners.listing');
    }
}
