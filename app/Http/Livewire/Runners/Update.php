<?php

namespace App\Http\Livewire\Runners;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Runners\RunnerCreator;
use App\Actions\Api\Tracks\ActiveTracks;
use App\Actions\Dto\Runners\SaveNewPayload;
use App\Models\Association;
use App\Models\Edition;
use App\Models\Runner;
use App\Models\Track;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use LivewireUI\Modal\ModalComponent;

class Update extends ModalComponent
{
    public int $runnerId = 0;
    public array $runner = [];

    public function mount(int $runnerId = 0)
    {
        $this->runnerId = $runnerId;

        $runner = Runner::with('person', 'association')->find($runnerId);
        $this->runner = $runner->toArray();
        $this->runner['opened_at'] = optional($runner->opened_at)->format('Y-m-d');
        $this->runner['closed_at'] = optional($runner->closed_at)->format('Y-m-d');
        $this->runner['person']['birthdate'] = optional($runner->person->birthdate)->format('Y');
    }

    public function edition(): Edition
    {
        return app()->make(CurrentEdition::class)->find();
    }

    public function requireOnlyName(): bool
    {
        return $this->edition()->require_only_name;
    }

    public function submit()
    {
        $runner = collect($this->runner);
        $payload = new SaveNewPayload;
        $payload->firstName = Arr::get($runner, 'person.first_name');
        $payload->lastName = Arr::get($runner, 'person.last_name');
        $payload->birthdate = Arr::get($runner, 'person.birthdate');
        $payload->sex = Arr::get($runner, 'person.sex');
        $payload->number = Arr::get($runner, 'number');
        $payload->family = Arr::get($runner, 'family', '') ?: '';
        $payload->trackId = Arr::get($runner, 'track_id');
        $payload->association = Arr::get($runner, 'association.name', '') ?: '';
        $payload->birthdate = $payload->birthdate
            ? Carbon::now()->setDay(1)->setMonth(1)->setYear($payload->birthdate)
            : null;

        app()->make(RunnerCreator::class)->update(Runner::find($this->runnerId), $payload);

        $this->emit(FastForm::SAVED_EVENT);
        $this->closeModal();
    }

    public function tracks(): array
    {
        $activeTracks = app()->make(ActiveTracks::class);

        return $activeTracks->find()
            ->map(fn(Track $track) => [
                'id' => $track->id,
                'name' => $track->name
            ])
            ->toArray();
    }

    public function associations(): array
    {
        return Association::query()
            ->orderBy('name')
            ->get(['name'])
            ->toArray();
    }

    public function families(): array
    {
        return Runner::query()
            ->distinct()
            ->whereNotNull('family')
            ->getQuery()
            ->get(['family'])
            ->pluck('family')
            ->values()
            ->toArray();
    }

    public function render()
    {
        return view('livewire.runners.update');
    }
}
