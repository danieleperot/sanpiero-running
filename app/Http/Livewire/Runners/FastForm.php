<?php

namespace App\Http\Livewire\Runners;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Api\Runners\RunnerCreator;
use App\Actions\Api\Tracks\ActiveTracks;
use App\Actions\Dto\Runners\SaveNewPayload;
use App\Models\Association;
use App\Models\Edition;
use App\Models\Runner;
use App\Models\Track;
use Carbon\Carbon;
use Livewire\Component;

class FastForm extends Component
{
    const SAVED_EVENT = 'runnerSaved';
    const SESSION_RUNNER_SAVED = 'runner.saved';

    public array $payload = [];

    public function tracks(): array
    {
        $activeTracks = app()->make(ActiveTracks::class);

        return $activeTracks->find()
            ->map(fn(Track $track) => [
                'id' => $track->id,
                'name' => $track->name
            ])
            ->toArray();
    }

    public function edition(): Edition
    {
        return app()->make(CurrentEdition::class)->find();
    }

    public function requireOnlyName(): bool
    {
        return $this->edition()->require_only_name;
    }

    public function associations(): array
    {
        return Association::query()
            ->orderBy('name')
            ->get(['name'])
            ->toArray();
    }

    public function families(): array
    {
        return Runner::query()
            ->distinct()
            ->whereNotNull('family')
            ->getQuery()
            ->get(['family'])
            ->pluck('family')
            ->values()
            ->toArray();
    }

    public function submit()
    {
        $payload = SaveNewPayload::from($this->payload);
        $payload->birthdate = $payload->birthdate
            ? Carbon::now()->setDay(1)->setMonth(1)->setYear($payload->birthdate)
            : null;

        $creator = app()->make(RunnerCreator::class);
        $creator->create($payload);

        $this->payload = [];
        session()->flash(self::SESSION_RUNNER_SAVED, 'Partecipante salvato correttamente');
        $this->emit(self::SAVED_EVENT);
    }

    public function render()
    {
        return view('livewire.runners.fast-form');
    }
}
