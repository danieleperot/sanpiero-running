<?php

namespace App\Http\Livewire\Runners;

use App\Models\Runner;
use Exception;
use LivewireUI\Modal\ModalComponent;

class Delete extends ModalComponent
{
    public int $runnerId = 0;

    public function mount(int $runnerId = 0)
    {
        $this->runnerId = $runnerId;

    }

    public function runner()
    {
        return Runner::find($this->runnerId);
    }

    public function submit()
    {
        if ($this->runner()->started_at)
            throw new Exception('Trying to delete already started runner.');

       $this->runner()->delete();

        $this->emit(FastForm::SAVED_EVENT);
        $this->emit('closeModal', true, [], true);
    }

    public function render()
    {
        return view('livewire.runners.delete');
    }
}
