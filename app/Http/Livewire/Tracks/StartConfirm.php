<?php

namespace App\Http\Livewire\Tracks;

use App\Models\Track;
use LivewireUI\Modal\ModalComponent;

class StartConfirm extends ModalComponent
{
    public int $trackId = 0;

    public function submit()
    {
        $track = Track::find($this->trackId);
        $track->runners()->update(['started_at' => now()]);

        activity()->performedOn($track)
            ->withProperties(['attributes' => ['started_at' => now()]])
            ->log('Gara cominciata');

        return redirect()->route('tracks.show', $this->trackId);
    }

    public function render()
    {
        return view('livewire.tracks.start-confirm');
    }
}
