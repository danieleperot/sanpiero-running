<?php

namespace App\Http\Livewire\Tracks;

use App\Models\Runner;
use App\Models\Track;
use LivewireUI\Modal\ModalComponent;

class RegisterArrival extends ModalComponent
{
    public const SAVED_EVENT = 'registered_arrival_saved';

    public int|string $number = 0;
    public int $trackId = 0;

    public function rules()
    {
        return [
            'number' => ['required', 'numeric', function ($attribute, $value, $fail) {
                $runner = Runner::whereTrackId($this->trackId)->whereNumber($value)->first();
                if (!$runner) return $fail('Pettorale non registrato in questa gara.');
                if ($runner->completed_at) return $fail('Il partecipante è già arrivato.');
            }]
        ];
    }

    public function submit()
    {
        $this->validate();

        $track = Track::find($this->trackId);
        $runner = $track->runners()->whereNumber($this->number)->first();

        $runner->update(['completed_at' => now()]);

        $this->number = 0;
        $this->emit(self::SAVED_EVENT);
    }

    public function render()
    {
        return view('livewire.tracks.register-arrival');
    }
}
