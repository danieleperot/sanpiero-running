<?php

namespace App\Http\Livewire\Tracks;

use App\Actions\Api\Runners\RunnersList as RunnersListAction;
use App\Actions\Dto\Runners\ListPayload;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\Component;

class RunnersList extends Component
{
    public int $page = 1;
    public ?int $trackId = null;
    public string $search = '';

    protected $queryString = ['page'];
    protected $listeners = [RegisterArrival::SAVED_EVENT => '$refresh'];

    public function mount(?int $trackId = null)
    {
        $this->trackId = $trackId;
    }

    public function changePage(int $page)
    {
        $this->page = $page;
    }

    public function columns(): array
    {
        return [
            ['label' => __('Pettorale'), 'traverse' => 'number'],
            [
                'label' => __('Cognome e nome'),
                'traverse' => 'id',
                'filter' => fn ($id, $runner) => "{$runner->person->last_name} {$runner->person->first_name}"
            ],
            [
                'label' => __('Associazione'),
                'traverse' => 'association.name'
            ],
            [
                'label' => __('Famiglia'),
                'traverse' => 'family'
            ],
            [
                'label' => __('Anno di nascita'),
                'traverse' => 'person.birthdate',
                'filter' => function ($value, $runner) {
                    if (!$value) return '';

                    return $runner->person->birthdate->format('Y')
                        . " ({$runner->person->birthdate->diffInYears(now())} anni)";
                }
            ],
            [
                'label' => __('Sesso'),
                'traverse' => 'person.sex',
                'filter' => function ($sex) {
                    if ($sex === 'f') return 'Donna';
                    if ($sex === 'm') return 'Uomo';

                    return '';
                }
            ],
            [
                'label' => __('Ora di partenza'),
                'traverse' => 'started_at',
                'filter' => function (?string $date) {
                    return $date ? Carbon::parse($date)->format('d/m/Y H:i:s') : '-';
                }
            ],
            [
                'label' => __('Ora di arrivo'),
                'traverse' => 'completed_at',
                'filter' => function ($completed, $runner) {
                    if (!$completed) return '-';
                    if (!$runner->started_at) return 'Attenzione! Percorso terminato senza partenza.';
                    $diff = Carbon::parse($completed)->diff($runner->started_at)->format('%Hh %Im %Ss');
                    $completed = Carbon::parse($completed)->format('d/m/Y H:i:s');
                    return "{$completed} (completato in {$diff})";
                }
            ]
        ];
    }

    public function runners(): LengthAwarePaginator
    {
        $request = new ListPayload($this->page, ListPayload::CURRENT_EDITON);
        if ($this->trackId) $request->trackId = $this->trackId;
        if ($this->search) $request->search = $this->search;

        $runnersList = app()->make(RunnersListAction::class);

        return $runnersList->get($request);
    }

    public function render()
    {
        return view('livewire.tracks.runners-list');
    }
}
