<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFamilyInfoToRunners extends Migration
{
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->string('family')->nullable();
        });
    }
}
