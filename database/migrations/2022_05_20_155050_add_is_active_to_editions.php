<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsActiveToEditions extends Migration
{
    public function up()
    {
        Schema::table('editions', function (Blueprint $table) {
            $table->boolean('is_active')->default(false);
        });
    }
}
