<?php

use App\Models\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPersonIdColumnToRunners extends Migration
{
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->foreignIdFor(Person::class);

            $table->unique(['person_id', 'track_id'], 'unique_person_for_track');
        });
    }
}
