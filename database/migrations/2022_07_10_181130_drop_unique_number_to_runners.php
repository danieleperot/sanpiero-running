<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropUniqueNumberToRunners extends Migration
{
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->dropUnique('unique_number_for_track');
        });
    }
}
