<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToAssociations extends Migration
{
    public function up()
    {
        Schema::table('associations', function (Blueprint $table) {
            $table->string('slug')->unique()->index();
        });
    }
}
