<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeOptionalFieldsNullableToPeople extends Migration
{
    public function up()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->dateTimeTz('birthdate')->nullable()->change();
        });

        Schema::table('people', function (Blueprint $table) {
            $table->string('sex')->nullable()->change();
        });
    }
}
