<?php

use App\Models\Track;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTrackIdColumnToRunners extends Migration
{
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->foreignIdFor(Track::class);
        });
    }
}
