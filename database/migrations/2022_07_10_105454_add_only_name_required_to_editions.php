<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnlyNameRequiredToEditions extends Migration
{
    public function up()
    {
        Schema::table('editions', function (Blueprint $table) {
            $table->boolean('require_only_name')->default(false);
        });
    }
}
