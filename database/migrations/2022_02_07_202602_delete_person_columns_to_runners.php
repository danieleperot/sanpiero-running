<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeletePersonColumnsToRunners extends Migration
{
    public function up()
    {
        Schema::table('runners', function (Blueprint $table) {
            $table->dropColumn('sex');
        });

        Schema::table('runners', function (Blueprint $table) {
            $table->dropColumn('first_name');
        });

        Schema::table('runners', function (Blueprint $table) {
            $table->dropColumn('last_name');
        });

        Schema::table('runners', function (Blueprint $table) {
            $table->dropColumn('birthday');
        });
    }

    public function down()
    {
        Schema::table('runners', function (Blueprint $table) {
            //
        });
    }
}
