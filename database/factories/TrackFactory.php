<?php /** @noinspection ALL */

namespace Database\Factories;

use App\Models\Edition;
use App\Models\Track;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Track create()
 * @method Track make()
 */
class TrackFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->numberBetween(4, 100) . ' KM',
            'edition_id' => fn () => Edition::factory()->create()
        ];
    }
}
