<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EditionFactory extends Factory
{
    public function definition(): array
    {
        $year = $this->faker->year();

        return [
            'created_at' => now()->year($year),
            'name' => 'Anno ' . $this->faker->year()
        ];
    }
}
