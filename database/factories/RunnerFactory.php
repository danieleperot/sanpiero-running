<?php

namespace Database\Factories;

use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method Runner create()
 * @method Runner make()
 */
class RunnerFactory extends Factory
{
    protected $model = Runner::class;

    public function definition(): array
    {
        return [
            'number' => $this->faker->unique()->numberBetween(1, 10000),
            'person_id' => fn () => Person::factory()->create(),
            'track_id' => fn () => Track::factory()->create()
        ];
    }
}
