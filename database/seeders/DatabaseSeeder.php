<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        User::firstOrCreate(['email' => 'perotdaniele@gmail.com'], [
            'email' => 'perotdaniele@gmail.com',
            'name' => 'Daniele Perot',
            'password' => Hash::make(Str::random())
        ]);
    }
}
