<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>Conferma iscrizione</title>
    <style>
        body {
            font-size: 0.65rem;
            color: #111827;
        }

        h1 {
            font-size: 0.7rem;
            margin-bottom: 1rem;
            font-weight: normal;
        }

        h2 {
            font-size: 0.685rem;
        }

        .section-logo {
            margin-bottom: 1rem;
        }

        .section-logo img {
            margin-left: 170px;
        }

        .section-logo div {
            text-align: center;
            font-size: 0.5rem;
            font-style: italic;
        }

        .assigned-number {
            text-align: center;
            margin: 1rem 0;
            font-size: 1.5rem;
            font-weight: bold;
        }

        section.with-border {
            border-top: 2px solid #94A3B8;
            padding-top: 0.5rem;
        }

        table {
            width: 470px;
            border-collapse: collapse;
            border: 1px solid #475569;
        }

        th {
            text-align: left;
            font-weight: normal;
            background: #94A3B8;
            padding: 0.125rem 0.5rem;
        }

        td {
            padding: 0.125rem 0.5rem;
        }

        td, th {
            border-right: 1px solid #475569;
        }

        td:last-child, th:last-child {
            border-right: none;
        }

        thead > tr {
            border-bottom: 1px solid #475569;
        }

        .thanks {
            margin-top: 2rem;
        }
    </style>
</head>
<body>
    <section class="section-logo">
        <img
            src="data:image/png;base64,{{ base64_encode(file_get_contents(public_path('/logo.png'))) }}"
            alt="Logo manifestazione"
            width="130px"
            height="130px"
        >
        <div>
            <div>San Piero Running - {{ now()->year }}</div>
            <div>Passeggiata non competitiva</div>
        </div>
    </section>

    <section>
        <h1 class="text-xl">Ciao <strong>{{ $runner->person->first_name }},</strong></h1>
        <p>
            La tua preiscrizione a San Piero Running - edizione {{ now()->year }}
            è stata completata con successo.
        </p>
        <p>
            Il giorno della manifestazione ti basterà semplicemente presentarti alla
            corsia preferenziale dedicata ai pre-iscritti per ritirare il tuo pettorale
            e pacco gara, e per versare la quota di partecipazione.
        </p>
    </section>

    <section>
        <p>
            Per la partecipazione alla gara "<strong>{{ $runner->track->name }}</strong>"
            ti è stato assegnato il pettorale:
        </p>
        <div class="assigned-number"># {{ $runner->number }}</div>
    </section>

    <section class="with-border">
        <h2>I tuoi dati</h2>
        <p>
            Riportiamo qui di seguito i dati con cui è stata effettuata la
            tua registrazione.
        </p>
    </section>
    <section>
        <table>
            <thead>
            <tr>
                <th>Cognome</th>
                <th>Nome</th>
                <th>Sesso</th>
                <th>Anno di nascita</th>
                <th>Associazione</th>
                @if ($runner->family)
                    <th>Famiglia</th>
                @endif
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>{{ $runner->person->last_name }}</td>
                <td>{{ $runner->person->first_name }}</td>
                <td>
                    {{ ['f' => 'Donna', 'm' => 'Uomo'][$runner->person->sex] ?? '' }}
                </td>
                <td>{{ optional($runner->person->birthdate)->format('Y') }} ({{ optional($runner->person->birthdate)->diffInYears(now()) }} anni)</td>
                <td>{{ optional($runner->association)->name ?: 'Senza associazione' }}</td>
                @if ($runner->family)
                    <td>{{ $runner->family }}</td>
                @endif
            </tr>
            </tbody>
        </table>
    </section>

    <section class="thanks">
        <h2>Grazie per la tua partecipazione!</h2>
        <p>
            Ti aspettiamo al Casello di Salzan di Santa Giustina (BL)
            @if ($runner->track->edition->opened_at ?? false)
                il giorno
                {{ $runner->track->edition->opened_at->format('d/m/Y') }}
            @endif
            a partire dalle ore 07:30. La partenza della gara è prevista per le
            ore 09:00.
        </p>
        <p>
            In caso di dubbi o domande puoi contattare l'indirizzo email
            <a href="mailto:dandalpan2@libero.it">dandalpan2@libero.it</a>,
            tramite Whatsapp ai numeri di telefono
            <a href="tel:3382588017">338 258 8017</a> e
            <a href="tel:3357971653">335 797 1653</a>, oppure la nostra pagina
            Facebook <a href="https://www.facebook.com/sanpierorunning">San Piero Running</a>.
        </p>
    </section>
</body>
</html>
