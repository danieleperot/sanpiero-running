@props(['item' => [], 'items' => [], 'column' => []])

<div class="flex items-center justify-center">
    <x-buttons.plain onclick="Livewire.emit('openModal', 'associations.update', { associationId: {{ $item->id }} })">
        Modifica
    </x-buttons.plain>
</div>
