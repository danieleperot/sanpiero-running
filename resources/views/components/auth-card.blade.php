<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        <h1 class="text-3xl font-semibold text-center mt-6 mb-12">
            San Piero Running
        </h1>
        <div>
            {{ $slot }}
        </div>
    </div>
</div>
