@props(['href' => '', 'text' => '', 'icon' => '', 'exact' => false])

@php
$active = $exact ? request()->url() === $href : Str::of(request()->url())->contains($href);
@endphp

<li>
    <a
        @class([
          "py-2 px-4 transition rounded-lg flex items-center space-x-4" => true,
          'hover:bg-gray-700 hover:text-white' => !$active,
          'bg-gray-900 text-white' => $active
         ])
        href="{{ $href }}"
    >
        @if ($icon)
            <x-svg-icon :name="$icon" class="w-5 h-5" />
        @endif
        <span>{{ $text }}</span>
    </a>
</li>
