@props(['title' => ''])

<x-app-layout :title="$title ? $title . ' - Gestisci' : 'Gestisci'">
    <div class="shadow-inner min-h-0 flex flex-grow">
        <aside class="w-full max-w-xs bg-gray-800 text-gray-300 py-12 px-3 space-y-8">
            <nav>
                <h2 class="font-medium text-xl px-2 mb-8 text-white">
                    Anagrafiche e statisiche
                </h2>
                <ul class="space-y-1">
                    {{-- <x-settings.menu-link text="Statistiche edizione" :href="route('reports.index')" icon="chart-pie" /> --}}
                    <x-settings.menu-link text="Tutti i partecipanti" :href="route('people.index')" icon="identification" />
                </ul>
            </nav>

            <nav>
                <h2 class="font-medium text-xl px-2 mb-8 text-white">
                    Impostazioni
                </h2>
                <ul class="space-y-1">
                    <x-settings.menu-link text="Edizioni" :href="route('editions.index')" icon="clipboard-list" />
                    <x-settings.menu-link text="Associazioni" :href="route('associations.index')" icon="user-group" />
                    <x-settings.menu-link text="Utenti" :href="route('users.index')" icon="user-circle" />
                    <x-settings.menu-link text="Logs e attività" :href="route('logs.index')" icon="presentation-chart-bar" />
                </ul>
            </nav>
        </aside>

        <div class="max-w-screen-2xl w-full mx-auto my-10 p-6">
            {{ $slot ?? '' }}
        </div>
    </div>
</x-app-layout>
