@props(['forTrack' => false])

<div {{ $attributes->merge(['class' => "bg-white w-full sm:flex"]) }}>
    <div class="grid gap-4 grid-cols-2 sm:grid-cols-3 @if ($forTrack) lg:grid-cols-6 @else lg:grid-cols-7 @endif lg:items-center w-full px-6 py-5">
        <div class="col-span-2 sm:col-span-1 sm:row-span-3 lg:row-span-1 pb-6 lg:pb-0 flex items-center lg:justify-start justify-center">
            <div class="text-lg text-gray-400">
                {{ $number }}
            </div>
        </div>
        <x-runners.row-property label="Cognome e nome">
            <x-slot name="value">
                <span class="inline-block">{{ $lastName }}</span>
                <span class="inline-block">{{ $firstName }}</span>
            </x-slot>
        </x-runners.row-property>
        @if (!$forTrack)
        <x-runners.row-property label="Percorso" :value="$track" />
        @endif
        <x-runners.row-property label="Sesso" :value="$sex" />
        <x-runners.row-property label="Anno di nascita">
            <x-slot name="value">
                <span class="inline-block">{{ $birthdate }}</span>
                @if ($age())
                    <span class="inline-block">({{ $age }})</span>
                @endif
            </x-slot>
        </x-runners.row-property>
        <x-runners.row-property label="Associazione" :value="$associationName" />
        <x-runners.row-property label="Famiglia" :value="$family" />
        @if ($forTrack)
        <x-runners.row-property label="Partenza" value="18:35:44" />
        <x-runners.row-property label="Arrivo" value="Non ancora arrivato" />
        @endif
    </div>
    @if (!$forTrack)
        <div class="flex items-center justify-center">
            <a
                href="{{ route('runners.download', ['runner' => $id]) }}"
                download="{{ $confirmDownloadName }}"
                class="cursor-pointer flex items-center justify-center flex-shrink-0 px-4 py-4 text-gray-400 transition hover:text-indigo-600 mx-auto sm:mr-0"
            >
                <span class="align-middle mr-2 inline-block sm:sr-only">Stampa conferma</span>
                <x-svg-icon name="printer" class="w-5 align-middle" />
            </a>

            <button
                class="flex items-center justify-center flex-shrink-0 px-4 py-4 text-gray-400 transition hover:text-indigo-600 mx-auto sm:mr-0"
                onclick="Livewire.emit('openModal', 'runners.update', { runnerId: {{ $id }} })"
            >
                <span class="align-middle mr-2 inline-block sm:sr-only">Visualizza</span>
                <x-svg-icon name="chevron-right" class="w-5 align-middle" />
            </button>
        </div>
    @endif
</div>

