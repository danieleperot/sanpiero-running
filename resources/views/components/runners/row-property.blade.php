@props(['label' => '', 'value' => ''])

<div>
    <div class="text-xs xs:text-sm text-gray-400">
        {{ $label }}
    </div>
    <div class="text-gray-600 text-sm xs:text-base">
        {{ $value }}
    </div>
</div>
