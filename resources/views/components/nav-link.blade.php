@props(['exact' => false, 'href' => ''])

@php
$active = $exact ? request()->url() === $href : Str::of(request()->url())->contains($href);
$classes = ($active ?? false)
            ? 'block sm:inline-flex items-center px-1 sm:px-3 py-2 sm:pt-1 border-b-2 border-indigo-400 text-sm font-medium leading-5 text-gray-900 focus:outline-none focus:border-indigo-700 transition duration-150 ease-in-out'
            : 'block sm:inline-flex items-center px-1 sm:px-3 py-2 sm:pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes, 'href' => $href]) }}>
    {{ $slot }}
</a>
