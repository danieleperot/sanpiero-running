@props(['target' => '', 'loader' => 'Caricamento in corso...'])

<div {!! $attributes->merge(['class' => 'relative']) !!}>
    {!! $slot ?? '' !!}

    <div
        wire:loading.flex
        @if ($target) wire:target="{{ $target }}" @endif
        class="absolute top-0 left-0 w-full h-full bg-white bg-opacity-70 flex items-center justify-center"
    >
        <x-svg-icon name="loading" class="w-10 h-10 text-indigo-600 animate-spin mr-6"/>
        <div class="text-xl font-semibold text-gray-400">
            {{ $loader }}
        </div>
    </div>
</div>
