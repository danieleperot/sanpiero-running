@php /** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */ @endphp
@props(['paginator'])

<div {{ $attributes->merge(['class' => 'text-gray-600 text-sm py-2']) }}>
    @if ($paginator->count())
        Risultati da <strong>{{ $paginator->firstItem() }}</strong> a
        <strong>{{ $paginator->lastItem() }}</strong>, per un totale di
        <strong>{{ $paginator->total() }}</strong> risultati.
    @else
        Nessun risultato trovato.
    @endif
</div>
