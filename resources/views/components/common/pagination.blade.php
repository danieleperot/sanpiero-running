@php /** @var \Illuminate\Pagination\LengthAwarePaginator $paginator */ @endphp
@props(['paginator','inLivewire' => false, 'baseUrl' => ''])

@php
    $current = $paginator->currentPage();
    $prev = 1;
    $next = min(5, $paginator->lastPage());

    if ($paginator->lastPage() > 5) {
        $limitPrev = $current - 2;
        $limitNext = $current + 2;

        $prev = max($limitPrev, 1);
        $next = min($limitNext, $paginator->lastPage());

        $missingPages = 5 - ($next - $prev) - 1;

        if ($current < 5) {$next += $missingPages;}
        if ($current > $paginator->lastPage() - 5) $prev -= $missingPages;
    }
@endphp

<nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px my-8" aria-label="Paginazione">
    @for($page = $prev; $page <= $next; $page++)
        <a
            @if ($current === $page)
                aria-disabled="true"
            aria-current="page"
            @else
                @if ($inLivewire)
                    wire:click.prevent="changePage({{ $page }})"
            @endif
            href="{{ $baseUrl ? $baseUrl . Arr::query(['page' => $page]) : $paginator->url($page) }}"
            @endif
            @class([
              "inline-flex relative items-center px-4 py-2 border text-sm font-medium first:rounded-l-md last:rounded-r-md" => true,
              "bg-indigo-50 border-indigo-500 text-indigo-600 z-10" => $paginator->currentPage() === $page,
              "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 " => $paginator->currentPage() !== $page
            ])
        >
            {{ $page }}
        </a>
    @endfor
</nav>
