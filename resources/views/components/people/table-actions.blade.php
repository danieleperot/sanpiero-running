@props(['item' => [], 'items' => [], 'column' => []])

<div class="flex items-center justify-center">
    <x-buttons.normal onclick="Livewire.emit('openModal', 'people.update', { personId: {{ $item->id }} })">
        Modifica
    </x-buttons.normal>
</div>
