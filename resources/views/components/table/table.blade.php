@props(['items' => [], 'columns' => [], 'actions' => ''])

<table class="block sm:table w-full sm:rounded sm:shadow sm:bg-white">
    <thead class="hidden sm:table-header-group shadow-sm border-b">
    @foreach ($columns as $column)
        <th class="hidden sm:table-cell px-4 py-4 text-left text-gray-500 uppercase text-sm font-semibold first:rounded-tl last:rounded-tr bg-gray-50">
            {{ $column['label'] }}
        </th>
    @endforeach
    @if ($actions)
        <th class="bg-gray-50 rounded-tr">{{-- Buttons --}}</th>
    @endif
    </thead>
    <tbody class="block sm:table-row-group space-y-8 sm:space-y-0">
    @foreach ($items as $item)
        <tr class="block sm:table-row border-b sm:last:border-b-0 border-gray-100 bg-white sm:bg-transparent shadow sm:shadow-none rounded sm:rounded-none border-y sm:border-t-0">
            @foreach ($columns as $column)
                <td class="block sm:table-cell px-4 py-4 text-left text-gray-600 text-sm first:bg-gray-50 sm:first:bg-transparent border-b border-gray-100 sm:border-b-none">
                    <div>
                        <div class="sm:hidden pb-1 font-medium text-xs text-gray-500">
                            {{ $column['label'] }}
                        </div>
                        @php
                        $value = Arr::get($item, $column['traverse'], '');
                        if ($column['filter'] ?? null)
                            $value = call_user_func($column['filter'], $value, $item)
                        @endphp
                        @if (($column['as'] ?? '') === 'json')
                        <div class="text-xs">
                            <pre>{!! json_encode($value ?: '', JSON_PRETTY_PRINT) !!}</pre>
                        </div>
                        @else
                        <div>
                            {!! $value ?: '-' !!}
                        </div>
                        @endif
                    </div>
                </td>
            @endforeach
            @if ($actions)
                <td>
                    <x-dynamic-component
                        :component="$actions"
                        :item="$item"
                        :items="$items"
                        :column="$column"
                    />
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
