@props(['href' => ''])
@php
$attributes = $attributes->merge([
    'class' => 'px-6 py-2 bg-indigo-600 text-white text-sm tracking-wide ring-offset-2
        hover:bg-indigo-800 transition-all font-semibold rounded shadow',
    'href' => $href
])

@endphp

@if ($href)
    <a {{ $attributes }}>{!! $slot ?? '' !!}</a>
@else
    <button {{ $attributes }}>{!! $slot ?? '' !!}</button>
@endif
