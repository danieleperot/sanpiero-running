@props(['href' => ''])
@php
$attributes = $attributes->merge([
    'class' => 'px-6 py-2 bg-white text-gray-600 text-sm tracking-wide ring-offset-2 border
        hover:bg-gray-800 hover:text-white transition-all font-semibold rounded shadow-sm',
    'href' => $href
])

@endphp

@if ($href)
    <a {{ $attributes }}>{!! $slot ?? '' !!}</a>
@else
    <button {{ $attributes }}>{!! $slot ?? '' !!}</button>
@endif
