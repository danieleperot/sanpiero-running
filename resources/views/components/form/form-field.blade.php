@props(['name', 'label', 'errorsName' => ''])
<label for="{{ $name }}" class="block w-full">
    <div class="text-gray-600 px-1 mb-px">{{ $label }}</div>
    <div>
        {!! $slot ?? '' !!}
    </div>
    @error($errorsName ?: $name)
    <div class="text-red-600 italic text-sm m-1">{{ $message }}</div>
    @enderror
</label>
