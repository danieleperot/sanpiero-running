@props(['name', 'label', 'errorsName' => '', 'withoutDefault' => false])
<x-form.form-field :label="$label" :name="$name" :errors-name="$errorsName">
    <select {{ $attributes->merge([
        'name' => $name,
        'class' => 'w-full border border-gray-200 rounded text-sm'
    ]) }}>
        @if (!$withoutDefault)
        <option selected hidden>Seleziona dalla lista</option>
        @endif
        {!! $slot ?? '' !!}
    </select>
</x-form.form-field>

