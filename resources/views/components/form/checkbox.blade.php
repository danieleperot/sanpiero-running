@props(['name', 'label', 'withoutDefault' => false])
<x-form.form-field :label="$label" :name="$name">
    <label class="flex items-start">
        <div>
        <input type="checkbox" {{ $attributes->merge([
            'name' => $name,
            'class' => 'inline-block mr-4 ml-1 border border-gray-200 rounded text-sm'
        ]) }}>
        </div>
        <span>
            {!! $slot ?? '' !!}
        </span>
    </label>
</x-form.form-field>

