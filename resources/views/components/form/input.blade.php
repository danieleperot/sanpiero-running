@props(['name', 'label'])
<x-form.form-field :label="$label" :name="$name">
    <input {{ $attributes->merge([
        'name' => $name,
        'class' => 'w-full border border-gray-200 rounded text-sm'
    ]) }}>
</x-form.form-field>

