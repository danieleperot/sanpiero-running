@props(['name', 'label'])
<x-form.form-field :label="$label" :name="$name">
    <input {{ $attributes->merge([
        'name' => $name,
        'list' => "{$name}-datalist",
        'class' => 'w-full border border-gray-200 rounded text-sm',
        'type' => 'text'
    ]) }}>
    <datalist id="{{ "{$name}-datalist" }}">
        {!! $slot ?? '' !!}
    </datalist>
</x-form.form-field>

