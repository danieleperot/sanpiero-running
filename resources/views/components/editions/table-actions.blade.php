@props(['item' => [], 'items' => [], 'column' => []])

<div class="flex items-center justify-center">
    <x-buttons.plain :href="route('editions.show', $item)">
        Modifica
    </x-buttons.plain>
</div>
