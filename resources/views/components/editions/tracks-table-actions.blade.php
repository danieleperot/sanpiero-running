@props(['item' => [], 'items' => [], 'column' => []])

<div class="flex items-center justify-center">
    <x-buttons.plain onclick="Livewire.emit('openModal', 'editions.tracks.update', { trackId: {{ $item->id }} })">
        Modifica
    </x-buttons.plain>
</div>
