<x-app-layout title="Dashboard">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-16">
            <div class="flex w-full items-center">
                <div class="p-2 bg-white shadow-lg rounded-lg mr-16 flex items-center justify-center">
                    <x-svg-icon name="sanpiero" class="w-36 h-36" />
                </div>

                <div class="w-full py-6">
                    <h1 class="text-4xl text-gray-600 font-semibold mb-6">
                        Benvenuto a San Piero Running
                    </h1>
                    @if ($edition->id)
                    <div class="text-gray-700">
                        Edizione in corso: <strong>{{ $edition->name }}.</strong>
                    </div>
                        @if ($edition->closed_at)
                        <div class="mt-1 text-gray-700">
                            L'edizione terminerà in data <strong>{{ $edition->closed_at->format('d/m/Y') }}.</strong>
                        </div>
                        @endif
                    @else
                    <div class="flex items-center">
                        <div class="mr-4">
                            Non ci sono al momento edizioni attive.
                        </div>
                        <x-buttons.plain :href="route('editions.index')">
                            Gestisci le edizioni
                        </x-buttons.plain>
                    </div>
                    @endif
                </div>
            </div>

            @if ($edition->id)
            <div>
                <h2 class="text-2xl text-gray-600 font-semibold mb-6">
                    Statistiche edizione
                </h2>
                <div class="grid grid-cols-2 gap-6">
                    <div class="bg-white shadow-md px-6 py-3 rounded-md">
                        <h3 class="text-gray-600 font-semibold text-sm">
                            Partecipanti attuali
                        </h3>
                        <div class="text-center text-indigo-600 text-6xl py-4">
                            {{ $edition->runners->count() }}
                        </div>
                    </div>
                    <div class="bg-white shadow-md px-6 py-3 rounded-md">
                        <h3 class="text-gray-600 font-semibold text-sm">
                            Percorsi
                        </h3>
                        <div class="text-center text-indigo-600 text-6xl py-4">
                            {{ $edition->tracks->count() }}
                        </div>
                    </div>
                </div>
                <div class="flex items-center justify-end mt-6 space-x-4">
                    <x-buttons.plain :href="route('runners.index')">
                        Gestisci partecipanti
                    </x-buttons.plain>
                </div>
            </div>
            @endif
        </div>
    </div>
</x-app-layout>
