<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="min-h-full flex flex-col flex-grow">
@include('partials.head', ['title' => $title])

<body class="font-sans antialiased min-h-0 flex flex-col flex-grow">
    <div class="min-h-screen bg-gray-100 min-h-0 flex flex-col flex-grow">
        @include('layouts.navigation')

        @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
        @endif

        <main class="min-h-0 flex flex-col flex-grow">
            {{ $slot }}
        </main>
    </div>

    @livewire('livewire-ui-modal')
    @livewireScripts
</body>
</html>
