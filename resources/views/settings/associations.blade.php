<x-settings.layout title="Associazioni">
    <div class="flex items-center justify-between mb-8">
        <h1 class="text-2xl text-gray-900 mr-4 font-medium">
            Tutte le associazioni
        </h1>
        <x-buttons.normal onclick="Livewire.emit('openModal', 'associations.create')">
            Aggiungi associazione
        </x-buttons.normal>
    </div>

    <livewire:associations.listing />
</x-settings.layout>
