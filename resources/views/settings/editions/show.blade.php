<x-settings.layout :title="$edition->name">
    <div class="flex items-center justify-between mb-8">
        <div>
            <h1 class="text-2xl text-gray-900 mr-4 font-medium">
                {{ $edition->name }}
            </h1>
            <a class="text-indigo-600 hover:text-indigo-900 transition mt-1" href="{{ route('editions.index') }}">
                Visualizza altre edizioni
            </a>
        </div>
        <div class="flex items-center space-x-2">
            <x-buttons.plain onclick="Livewire.emit('openModal', 'editions.update', { editionId: {{ $edition->id }} })">
                Modifica
            </x-buttons.plain>
            <x-buttons.plain download :href="route('editions.download', $edition)">
                Scarica dati
            </x-buttons.plain>
        </div>
    </div>

    <div class="flex items-center justify-between mb-8">
        <h2 class="text-xl text-gray-900 mr-4 font-medium">
            Percorsi per l'edizione
        </h2>
        <x-buttons.normal onclick="Livewire.emit('openModal', 'editions.tracks.create', { 'editionId': {{ $edition->id }} })">
            Aggiungi percorso
        </x-buttons.normal>
    </div>
    <x-table.table
        :columns="$columns"
        :items="$tracks"
        actions="editions.tracks-table-actions"
    />
</x-settings.layout>
