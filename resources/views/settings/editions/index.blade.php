<x-settings.layout title="Edizioni">
    <div class="flex items-center justify-between mb-8">
        <h1 class="text-2xl text-gray-900 mr-4 font-medium">
            Tutte le edizioni
        </h1>
        <x-buttons.normal onclick="Livewire.emit('openModal', 'editions.create-form')">
            Aggiungi edizione
        </x-buttons.normal>
    </div>

    <livewire:editions.listing/>
</x-settings.layout>
