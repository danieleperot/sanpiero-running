<x-settings.layout title="Utenti">
    <div class="flex items-center justify-between mb-8">
        <h1 class="text-2xl text-gray-900 mr-4 font-medium">
            Tutte gli utenti
        </h1>
        <x-buttons.normal onclick="Livewire.emit('openModal', 'users.create')">
            Aggiungi utente
        </x-buttons.normal>
    </div>

    <livewire:users.listing />
</x-settings.layout>
