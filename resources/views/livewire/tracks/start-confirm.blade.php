<div class="mx-auto w-full max-w-md bg-white rounded-2xl shadow-xl">
    <form wire:submit.prevent="submit">
        <div class="py-6 px-8">
            Vuoi davvero cominciare questa gara? L'operazione non è reversible.
        </div>

        <div class="flex justify-end items-center space-x-2 px-4 pb-4">
            <button type="button" class="text-sm p-4" wire:click="$emit('closeModal')">
                Chiudi
            </button>
            <x-buttons.normal>Comincia gara</x-buttons.normal>
        </div>
    </form>
</div>
