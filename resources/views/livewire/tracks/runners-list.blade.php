<x-common.livewire-loader>
    <div class="my-4 flex ">
        <div>
            <x-form.input type="text" wire:model.debounce.500ms="search" name="search" label="Cerca partecipante" />
        </div>
    </div>

    <x-table.table
        :columns="$this->columns()"
        :items="$this->runners()->items()"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->runners()" />
        <x-common.pagination :paginator="$this->runners()" in-livewire :base-url="'/tracks/' . $this->trackId . '?'" />
    </div>
</x-common.livewire-loader>
