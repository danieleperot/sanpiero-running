<x-common.livewire-loader loader="Salvataggio partecipante..." target="submit">
    <div class="bg-white rounded-xl shadow-xl">
        <div class="py-5 px-4 bg-slate-50 rounded-t-xl shadow-sm border-b">
            <div class="text-xl font-medium text-gray-900">
                Modifica partecipante
            </div>
        </div>
        <form wire:submit.prevent="submit">
            <div class="px-4 py-6 space-y-4">
                <fieldset class="grid sm:grid-cols-2 gap-6">
                    <x-form.input type="text" required name="lastName" label="Cognome" placeholder="Rossi" autofocus wire:model="person.last_name" />
                    <x-form.input type="text" required name="firstName" label="Nome" placeholder="Mario" wire:model="person.first_name" />
                    <x-form.input type="number" required name="birthdate" min="1915" max="{{ now()->year }}" label="Anno di nascita" placeholder="1970" wire:model="person.birthdate" />
                    <x-form.select name="sex" required label="Sesso" wire:model="person.sex">
                        <option value="f">Donna</option>
                        <option value="m">Uomo</option>
                    </x-form.select>
                </fieldset>
            </div>
            <div class="flex justify-end items-center space-x-6 p-4">
                <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                    Chiudi
                </button>
                <x-buttons.normal>Salva</x-buttons.normal>
            </div>
        </form>
    </div>
</x-common.livewire-loader>
