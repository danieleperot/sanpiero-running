<x-common.livewire-loader>
    <x-table.table
        :columns="$this->columns()"
        :items="$this->people()->items()"
        actions="people.table-actions"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->people()" />
        <x-common.pagination :paginator="$this->people()" in-livewire base-url="/settings/people?" />
    </div>
</x-common.livewire-loader>
