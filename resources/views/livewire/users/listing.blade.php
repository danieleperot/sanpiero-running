<x-common.livewire-loader>
    <x-table.table
        :columns="$this->columns()"
        :items="$this->users()->items()"
        actions="users.table-actions"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->users()" />
        <x-common.pagination :paginator="$this->users()" in-livewire base-url="/settings/users?" />
    </div>
</x-common.livewire-loader>
