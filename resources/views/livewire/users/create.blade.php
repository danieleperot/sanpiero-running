<div class="bg-white rounded-xl shadow-xl">
    <div class="py-5 px-4 bg-slate-50 shadow-sm border-b rounded-t-xl">
        <div class="text-xl font-medium text-gray-900">
            Aggiungi utente
        </div>
    </div>
    <form wire:submit.prevent="submit">
        <div class="px-4 py-6 space-y-4">
            <div class="mb-6 text-gray-800">
                Inserisci i dati del nuovo utente. Il nuovo utente potrà accedere
                al gestionale, e visualizzare e modificare i dati. Ricorda di comunicare
                la password e l'indirizzo email all'utente una volta modificati.
            </div>
            <fieldset class="grid gap-4 grid-cols-2">
                <x-form.input
                    wire:model="user.name"
                    label="Nome completo"
                    name="user.name"
                    placeholder="Mario Rossi"
                    type="text"
                    required
                />
                <x-form.input
                    wire:model="user.email"
                    label="Indirizzo email"
                    name="user.email"
                    placeholder="mario.rossi@example.com"
                    type="text"
                    required
                />
                <x-form.input
                    wire:model="user.password"
                    label="Password"
                    name="user.password"
                    placeholder="PasswordMoltoSicura10!"
                    type="password"
                    required
                />
                <x-form.input
                    wire:model="user.password_confirmation"
                    label="Conferma password"
                    name="user.password_confirmation"
                    placeholder="PasswordMoltoSicura10!"
                    type="password"
                    required
                />
                <div class="col-span-2">
                    <x-form.select
                        wire:model="user.role"
                        label="Ruolo"
                        name="user.role"
                        required
                    >
                        @foreach (\App\Enums\UserRole::cases() as $case)
                            <option value="{{ $case->value }}">{{ $case->label() }}</option>
                        @endforeach
                    </x-form.select>
                    <ul class="text-sm text-gray-600 list-disc pl-2 ml-4 mt-2">
                        @foreach (\App\Enums\UserRole::cases() as $case)
                            <li><strong>{{ $case->label() }}:</strong> {{ $case->description() }}</li>
                        @endforeach
                    </ul>
                </div>
            </fieldset>
        </div>
        <div class="flex justify-end items-center space-x-6 p-4">
            <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                Chiudi
            </button>
            <x-buttons.normal>Aggiungi utente</x-buttons.normal>
        </div>
    </form>
</div>
