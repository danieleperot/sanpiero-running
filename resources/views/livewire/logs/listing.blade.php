<x-common.livewire-loader>
    <x-table.table
        :columns="$this->columns()"
        :items="$this->logs()->items()"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->logs()" />
        <x-common.pagination :paginator="$this->logs()" in-livewire base-url="/settings/logs?" />
    </div>
</x-common.livewire-loader>
