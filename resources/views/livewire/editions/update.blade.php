<div class="bg-white rounded-xl shadow-xl">
    <div class="py-5 px-4 bg-slate-50 rounded-t-xl shadow-sm border-b">
        <div class="text-xl font-medium text-gray-900">
            Modifica edizione
        </div>
    </div>
    <form wire:submit.prevent="submit">
        <div class="px-4 py-6 space-y-4">
            <x-form.input wire:model="edition.name" label="Nome edizione" name="edition.name" placeholder="Terza edizione - 2022" type="text" required />
            <x-form.input wire:model="edition.opened_at" label="Data di apertura" name="edition.opened_at" type="date" />
            <x-form.input wire:model="edition.closed_at" label="Data di chiusura" name="edition.closed_at" type="date" />
            <x-form.checkbox wire:model="edition.require_only_name" label="Campi obbligatiori" name="edition.require_only_name">
                Se attivato, rendi obbligatorio solo il nome e cognome.
                Se disattivato, rendi obbligatori nome, cognome, anno di nascita e pettorale.
            </x-form.checkbox>
            <x-form.checkbox wire:model="edition.is_active" label="Edizione corrente" name="edition.is_active">
                Imposta questa edizione come in corso
            </x-form.checkbox>
        </div>
        <div class="flex justify-end items-center space-x-6 p-4">
            <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                Chiudi
            </button>
            <x-buttons.normal>Salva</x-buttons.normal>
        </div>
    </form>
</div>
