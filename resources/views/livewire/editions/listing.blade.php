<x-common.livewire-loader>
    <x-table.table
        :columns="$this->columns()"
        :items="$this->editions()->items()"
        actions="editions.table-actions"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->editions()" />
        <x-common.pagination :paginator="$this->editions()" in-livewire base-url="/settings/editions?" />
    </div>
</x-common.livewire-loader>
