<div class="bg-white rounded-xl shadow-xl">
    <div class="py-5 px-4 bg-slate-50 shadow-sm border-b rounded-t-xl">
        <div class="text-xl font-medium text-gray-900">
            Aggiungi percorso
        </div>
    </div>
    <form wire:submit.prevent="submit">
        <div class="px-4 py-6 space-y-4">
            <div class="mb-6 text-gray-800">
                Inserisci il nome del percorso per l'edizione corrente. Potrai
                registrare i partecipanti successivamente.
            </div>
            <x-form.input wire:model="track.name" label="Nome percorso" name="track.name" placeholder="Val Scura Short Trail" type="text" required />
        </div>
        <div class="flex justify-end items-center space-x-6 p-4">
            <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                Chiudi
            </button>
            <x-buttons.normal>Aggiungi edizione</x-buttons.normal>
        </div>
    </form>
</div>
