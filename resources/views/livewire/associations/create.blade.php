<div class="bg-white rounded-xl shadow-xl">
    <div class="py-5 px-4 bg-slate-50 shadow-sm border-b rounded-t-xl">
        <div class="text-xl font-medium text-gray-900">
            Aggiungi associazione
        </div>
    </div>
    <form wire:submit.prevent="submit">
        <div class="px-4 py-6 space-y-4">
            <div class="mb-6 text-gray-800">
                Inserisci il nome dell'associazione. Potrai poi iscrivere i
                partecipanti usando la nuova associazione.
            </div>
            <x-form.input
                wire:model="association.name"
                label="Nome associazione"
                name="association.name"
                placeholder="A.R.C. Salzan"
                type="text"
                required
            />
        </div>
        <div class="flex justify-end items-center space-x-6 p-4">
            <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                Chiudi
            </button>
            <x-buttons.normal>Aggiungi associazione</x-buttons.normal>
        </div>
    </form>
</div>
