<x-common.livewire-loader>
    <x-table.table
        :columns="$this->columns()"
        :items="$this->associations()->items()"
        actions="associations.table-actions"
    />

    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg">
    </div>

    <div class="flex items-center justify-between">
        <x-common.pagination-info :paginator="$this->associations()" />
        <x-common.pagination :paginator="$this->associations()" in-livewire base-url="/settings/associations?" />
    </div>
</x-common.livewire-loader>
