<x-common.livewire-loader loader="Salvataggio partecipante..." target="submit">
    <div class="bg-white rounded-xl shadow-xl">
        <div class="py-5 px-4 bg-slate-50 rounded-t-xl shadow-sm border-b">
            <div class="text-xl font-medium text-gray-900">
                Modifica partecipante
            </div>
        </div>
        <form wire:submit.prevent="submit">
            <div class="px-4 py-6 space-y-4">
                <fieldset class="grid sm:grid-cols-2 gap-6">
                    <x-form.input type="text" required name="lastName" label="Cognome" placeholder="Rossi" autofocus wire:model="runner.person.last_name" />
                    <x-form.input type="text" required name="firstName" label="Nome" placeholder="Mario" wire:model="runner.person.first_name" />
                    <x-form.input type="number" :required="!$this->requireOnlyName()" name="birthdate" min="1915" max="{{ now()->year }}" label="Anno di nascita" placeholder="1970" wire:model="runner.person.birthdate" />
                    <x-form.select name="sex" :required="!$this->requireOnlyName()" label="Sesso" wire:model="runner.person.sex">
                        <option value="f">Donna</option>
                        <option value="m">Uomo</option>
                    </x-form.select>
                </fieldset>
                <fieldset class="grid sm:grid-cols-2 gap-6">
                    <x-form.input type="number" :required="!$this->requireOnlyName()" name="number" label="Pettorale" placeholder="1234" wire:model="runner.number" />
                    <x-form.select name="track" required label="Percorso" wire:model="runner.track_id">
                        @foreach($this->tracks() as $track)
                            <option value="{{ $track['id'] }}">{{ $track['name'] }}</option>
                        @endforeach
                    </x-form.select>
                    <x-form.datalist name="association" label="Associazione" placeholder="Lascia vuoto se senza associazione" wire:model="runner.association.name">
                        @foreach($this->associations() as $association)
                            <option value="{{ $association['name'] }}"></option>
                        @endforeach
                    </x-form.datalist>
                    <x-form.datalist name="family" label="Famiglia" placeholder="Lascia vuoto se non specificato" wire:model="runner.family">
                        @foreach($this->families() as $family)
                            <option value="{{ $family }}"></option>
                        @endforeach
                    </x-form.datalist>
                </fieldset>
            </div>
            <div class="flex justify-between items-center space-x-6 p-4">
                <x-buttons.delete
                    type="button"
                    :disabled="!!$runner['started_at']"
                    :title="$runner['started_at'] ? 'Non è possibile rimuovere un partecipante che ha già cominciato la gara' : ''"
                    onclick="Livewire.emit('openModal', 'runners.delete', { runnerId: {{ $runnerId }} })"
                >
                    Elimina
                </x-buttons.delete>
                <div class="flex justify-end items-center space-x-6">
                    <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                        Chiudi
                    </button>
                    <x-buttons.normal>Salva</x-buttons.normal>
                </div>
            </div>
        </form>
    </div>
</x-common.livewire-loader>
