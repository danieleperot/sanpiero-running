<x-common.livewire-loader loader="Salvataggio partecipante..." target="submit">
    <form method="post" wire:submit.prevent="submit">
        @csrf
        <div class="space-y-6">
            <div class="flex items-center justify-between">
                <h3 class="text-gray-700 text-lg font-semibold mr-2">Aggiungi partecipante</h3>
                @if (session()->has($this::SESSION_RUNNER_SAVED))
                    <div class="flex items-center text-sm">
                        <x-svg-icon name="check-circle" class="w-4 h-4 mr-2 flex-shrink-0 text-green-600" />
                        <div class="text-gray-500 italic">Partecipante aggiunto con successo</div>
                    </div>
                @endif
            </div>
            <fieldset class="grid sm:grid-cols-4 gap-6">
                <x-form.input type="text" required name="lastName" label="Cognome" placeholder="Rossi" autofocus wire:model="payload.lastName" />
                <x-form.input type="text" required name="firstName" label="Nome" placeholder="Mario" wire:model="payload.firstName" />
                <x-form.input type="number" :required="!$this->requireOnlyName()" name="birthdate" min="1915" max="{{ now()->year }}" label="Anno di nascita" placeholder="1970" wire:model="payload.birthdate" />
                <x-form.select name="sex" :required="!$this->requireOnlyName()" label="Sesso" wire:model="payload.sex">
                    <option value="f">Donna</option>
                    <option value="m">Uomo</option>
                </x-form.select>
            </fieldset>
            <fieldset class="grid sm:grid-cols-4 gap-6">
                <x-form.input type="number" :required="!$this->requireOnlyName()" name="number" label="Pettorale" placeholder="1234" wire:model="payload.number" />
                <x-form.select name="track" errors-name="trackId" required label="Percorso" wire:model="payload.trackId">
                @foreach($this->tracks() as $track)
                    <option value="{{ $track['id'] }}">{{ $track['name'] }}</option>
                @endforeach
                </x-form.select>
                <x-form.datalist name="association" label="Associazione" placeholder="Lascia vuoto se senza associazione" wire:model="payload.association">
                    @foreach($this->associations() as $association)
                        <option value="{{ $association['name'] }}"></option>
                    @endforeach
                </x-form.datalist>
                <x-form.datalist name="family" label="Famiglia" placeholder="Lascia vuoto se non specificato" wire:model="payload.family">
                    @foreach($this->families() as $family)
                        <option value="{{ $family }}"></option>
                    @endforeach
                </x-form.datalist>
            </fieldset>
            <div class="flex items-center justify-end">
                <x-buttons.normal type="submit">Aggiungi</x-buttons.normal>
            </div>
        </div>
    </form>
</x-common.livewire-loader>
