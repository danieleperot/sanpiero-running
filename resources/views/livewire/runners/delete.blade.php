<x-common.livewire-loader loader="Eliminazione partecipante..." target="submit">
    <div class="bg-white rounded-xl shadow-xl">
        <div>
            <div class="sm:flex sm:items-start px-4 py-6 ">
                <div
                    class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                    <!-- Heroicon name: outline/exclamation -->
                    <svg class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                         stroke-width="2" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"/>
                    </svg>
                </div>
                <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">Elimina partecipante</h3>
                    <div class="mt-2 text-sm text-gray-600">
                        <p>
                            Vuoi davvero rimuovere il partecipante da questa edizione?
                            Non potrai più registrare i tempi e il pettorale sarà di nuovo disponibile.
                        </p>
                        <p>
                            Questa operazione verrà registrata, e non potrà essere
                            annullata una volta confermata.
                        </p>
                    </div>
                </div>
            </div>
            <div class="flex justify-end items-center space-x-6 p-4">
                <button type="button" class="text-sm" wire:click="$emit('closeModal')">
                    Chiudi
                </button>
                <x-buttons.delete wire:click="submit">
                    Elimina
                </x-buttons.delete>
            </div>
        </div>
    </div>
</x-common.livewire-loader>
