<x-common.livewire-loader>
    <div class="my-4 flex items-center justify-end">
        <div>
        <x-form.input type="text" wire:model.debounce.500ms="search" name="search" label="Cerca partecipante" />
        </div>
    </div>
    @if ($this->runners()->count())
    <div class="divide-y-2 divide-gray-100 bg-white shadow rounded-lg px-2">
        @foreach ($this->runners()->items() as $runner)
            <x-runners.row
                :runner="$runner"
                :for-track="!!$this->trackId"
                class="first:rounded-t-lg last:rounded-b-lg"
            />
        @endforeach
    </div>
    <div class="flex items-center justify-between px-4">
        <x-common.pagination-info :paginator="$this->runners()" />
        <x-common.pagination :paginator="$this->runners()" in-livewire base-url="/runners?" />
    </div>
    @else
    <div class="flex items-center justify-center flex-col space-y-4 py-12 px-4">
        <div class="text-gray-400 text-3xl font-semibold">
            Non ci sono partecipanti per questa edizione!
        </div>
        @if ($editionId)
        <div class="text-gray-600 text-lg">
            Comincia a registrare qualche partecipante nel modulo qui sopra.
        </div>
        @else
        <div class="flex items-center justify-center">
            <div class="mr-4">
                Non ci sono al momento edizioni attive.
            </div>
            <x-buttons.plain :href="route('editions.index')">
                Gestisci le edizioni
            </x-buttons.plain>
        </div>
        @endif
    </div>
    @endif
</x-common.livewire-loader>
