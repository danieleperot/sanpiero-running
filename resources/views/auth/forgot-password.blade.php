<x-guest-layout>
    <x-auth-card>
        <div class="mb-4 text-sm text-gray-600">
            {{ __("Hai dimenticato la tua password? Nessun problema. Inserisci il tuo indirizzo email e ti invieremo il link di reset della password.") }}
        </div>

        <x-auth-session-status class="mb-4" :status="session('status')" />
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div>
                <x-label for="email" :value="__('Indirizzo email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button>
                    {{ __("Invia il link di reset") }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
