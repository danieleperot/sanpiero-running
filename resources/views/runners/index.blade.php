<x-app-layout title="Partecipanti">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Partecipanti') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-12">
            @if ($edition->id)
            <div class="bg-white rounded-lg shadow p-4">
                <livewire:runners.fast-form />
            </div>
            @endif
            <div>
                <livewire:runners.listing :edition-id="$edition->id" />
            </div>
        </div>
    </div>
</x-app-layout>
