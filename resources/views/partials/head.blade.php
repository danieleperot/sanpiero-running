@php $title = $title ?? null @endphp

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if ($title)
            {!! $title !!} |
        @endif
        {{ config('app.name', 'San Piero Running') }}
    </title>

    <link rel="icon" href="/favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/app.js') }}" defer></script>

    @livewireStyles
</head>
