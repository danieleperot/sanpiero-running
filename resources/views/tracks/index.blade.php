<x-app-layout title="Percorsi">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Percorsi di questa edizione') }}
        </h2>
    </x-slot>

    <div class="py-12">
        @if (!$edition->id)
            <div class="flex items-center mx-auto max-w-7xl px-6 w-full justify-center">
                <div class="mr-4">
                    Non ci sono al momento edizioni attive.
                </div>
                <x-buttons.plain :href="route('editions.index')">
                    Gestisci le edizioni
                </x-buttons.plain>
            </div>
        @endif

        <div class="max-w-7xl mx-auto grid sm:grid-cols-2 gap-8 px-6">
            @foreach($tracks as $track)
                <div class="bg-white shadow-md rounded-xl flex items-center flex-col justify-center pt-4">
                    <h2 class="text-3xl text-indigo-600 mt-4 text-center px-4">{{ $track->name }}</h2>
                    <div class="text-center text-lg mt-2 mb-8 text-gray-700 px-4">
                        {{ $track->runners_count }} partecipanti
                    </div>
                    <a
                        href="{{ route('tracks.show', $track) }}"
                        class="mt-auto justify-self-end self-end border-t w-full px-4 flex justify-end py-3 text-gray-600 hover:text-indigo-600 items-center"
                    >
                        <span class="align-middle flex-shrink-0 mr-4">Visualizza</span>
                        <x-svg-icon name="chevron-right" class="w-4 h-4 align-middle" />
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</x-app-layout>
