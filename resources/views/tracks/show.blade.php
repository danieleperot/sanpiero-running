<x-app-layout title="{{ $track->name . ' - Percorsi' }}">
    <x-slot name="header">
        <div class="flex items-center justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Percorso :name', ['name' => $track->name]) }}
            </h2>
            <x-buttons.normal :href="route('tracks.index')">
                Torna ai percorsi
            </x-buttons.normal>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto px-6">
            <div class="flex justify-end mb-6">
            @if (!optional($track->runners->first())->started_at)
                <x-buttons.normal
                    onclick="Livewire.emit('openModal', 'tracks.start-confirm', { trackId: {{ $track->id }} })"
                >
                    Comincia gara
                </x-buttons.normal>
            @else
                <x-buttons.normal
                    onclick="Livewire.emit('openModal', 'tracks.register-arrival', { trackId: {{ $track->id }} })"
                >
                    Registra arrivi
                </x-buttons.normal>
            @endif
            </div>
            <livewire:tracks.runners-list :track-id="$track->id" />
        </div>
    </div>
</x-app-layout>
