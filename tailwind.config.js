const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
    './vendor/wire-elements/modal/resources/views/*.blade.php',
    './storage/framework/views/*.php',
    './resources/views/**/*.blade.php',
    './resources/views/**/*.vue',
    './resources/views/**/*.css',
    './resources/svg/**/*.svg'
  ],

  theme: {
    extend: {
      colors: {
        slate: colors.coolGray
      },
      boxShadow: {
        inner: 'inset 4px 1px 2px 0px rgba(0, 0, 0, 0.1)'
      }
    }
  },

  variants: {
    extend: {}
  },

  plugins: [require('@tailwindcss/forms')]
}
