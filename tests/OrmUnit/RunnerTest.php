<?php

namespace Tests\OrmUnit;

use App\Models\Association;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RunnerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function runner_belongs_to_track()
    {
        $runner = Runner::factory()->make();
        $track = Track::factory()->create();

        $runner->track()->associate($track);
        $runner->save();

        $this->assertEquals($track->id, $runner->fresh()->track_id);
    }

    /** @test */
    public function runner_has_number()
    {
        $runner = Runner::factory()->create();

        $runner->number = 12345;
        $runner->save();

        $this->assertEquals(12345, $runner->fresh()->number);
    }

    /** @test */
    public function cannot_save_in_database_two_runners_with_same_number()
    {
        $track = Track::factory()->create();
        $runner = Runner::factory()->for($track)->make();
        $conflict = Runner::factory()->for($track)->make();

        $runner->forceFill(['number' => 999])->save();

        $this->expectException(QueryException::class);
        $conflict->forceFill(['number' => 999])->save();
    }

    /** @test */
    public function can_save_two_runners_with_same_number_if_on_different_tracks()
    {
        $this->expectNotToPerformAssertions();

        $runner = Runner::factory()->for(Track::factory())->make();
        $notConflict = Runner::factory()->for(Track::factory())->make();

        $runner->forceFill(['number' => 999])->save();
        $notConflict->forceFill(['number' => 999])->save();
    }

    /** @test */
    public function runner_is_a_person()
    {
        $person = Person::factory()->create();
        $runner = Runner::factory()->for($person)->create();

        $this->assertEquals($person->id, $runner->fresh()->person_id);
    }

    /** @test */
    public function only_unique_runner_person_for_track()
    {
        $track = Track::factory()->create();
        $person = Person::factory()->create();
        $runner1 = Runner::factory()->for($track)->make();
        $runner2 = Runner::factory()->for($track)->make();

        $runner1->person()->associate($person)->save();
        $runner2->person()->associate($person);

        $this->expectException(QueryException::class);
        $runner2->save();
    }

    /** @test */
    public function runner_can_belong_to_association()
    {
        $runner = Runner::factory()->make();
        $association = Association::factory()->create();

        $runner->association()->associate($association);
        $runner->save();

        $this->assertEquals($association->id, $runner->fresh()->association_id);
    }
}
