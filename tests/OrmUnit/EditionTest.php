<?php

namespace Tests\OrmUnit;

use App\Models\Edition;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function edition_exists()
    {
        Edition::factory()->create();

        $this->assertCount(1, Edition::all());
     }

     /** @test */
     public function edition_has_many_tracks()
     {
         $edition = Edition::factory()->create();

         $track1 = Track::factory()->for($edition)->create();
         $track2 = Track::factory()->for($edition)->create();
         $otherEditionTrack = Track::factory()->create();
         $edition->refresh();

         $editionTracks = $edition->tracks->pluck('id')->toArray();
         $this->assertNotContains($otherEditionTrack->id, $editionTracks);
         $this->assertEquals([$track1->id, $track2->id], $editionTracks);
     }

     /** @test */
     public function has_many_runners()
     {
         $edition = Edition::factory()->create();

         $track1 = Track::factory()->for($edition)->create();
         $otherTrack = Track::factory()->create();

         $runners = Runner::factory()->for($track1)->count(3)->create();
         $others = Runner::factory()->for($otherTrack)->count(5)->create();

         $edition->refresh();

         $editionRunners = $edition->runners->pluck('id')->toArray();
         $this->assertNotContains($others->pluck('id')->toArray(), $editionRunners);
         $this->assertEqualsCanonicalizing($runners->pluck('id')->toArray(), $editionRunners);
     }
}
