<?php

namespace Tests\OrmUnit;

use App\Models\Association;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PersonTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_be_created_with_person_attributes()
    {
        $person = new Person;
        $person->first_name = '::first name::';
        $person->last_name = '::last name::';
        $person->birthdate = '1970-01-01';
        $person->sex = 'm';

        $this->expectNotToPerformAssertions();
        $person->save();
    }

    /** @test */
    public function can_be_many_runners()
    {
        [$person1, $person2] = Person::factory()->count(2)->create();

        Runner::factory()->count(3)->for($person1)->create();
        Runner::factory()->count(2)->for($person2)->create();

        $this->assertEquals(3, $person1->fresh()->runners()->count());
        $this->assertEquals(2, $person2->fresh()->runners()->count());
    }

    /** @test */
    public function can_participate_in_many_tracks()
    {
        $person = Person::factory()->create();
        $otherPerson = Person::factory()->create();
        $track1 = Track::factory()->create();
        $track2 = Track::factory()->create();
        $track3 = Track::factory()->create();

        Runner::factory()->for($track1)->for($person)->create();
        Runner::factory()->for($track2)->for($person)->create();
        Runner::factory()->for($track3)->for($otherPerson)->create();

        $ranInTracks = $person->fresh()->tracks->pluck('id')->toArray();
        $this->assertEquals([$track1->id, $track2->id], $ranInTracks);
    }

    /** @test */
    public function person_can_be_part_of_many_associations()
    {
        $person = Person::factory()->create();
        [$a1, $a2] = Association::factory()->count(2)->create();
        $a3 = Association::factory()->create();

        Runner::factory()->for($person)->for($a1)->create();
        Runner::factory()->for($person)->for($a2)->create();
        Runner::factory()->for($a3)->create();

        $associations = $person->fresh()->associations->pluck('id')->toArray();
        $this->assertEquals([$a1->id, $a2->id], $associations);
    }
}
