<?php

namespace Tests\OrmUnit;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Eloquent\Tracks\ActiveTracks;
use App\Models\Edition;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class EloquentActiveTracksTest extends TestCase
{
    use RefreshDatabase;

    /** @var MockObject|CurrentEdition */
    private $currentEdition;

    public function setUp(): void
    {
        parent::setUp();

        $this->currentEdition = $this->getMockBuilder(CurrentEdition::class)->getMock();
    }

    /** @test */
    public function given_no_edition_then_it_returns_no_tracks()
    {
        $activeTracks = new ActiveTracks($this->currentEdition);
        $result = $activeTracks->find();

        $this->assertEquals([], $result->toArray());
    }

    /** @test */
    public function if_more_than_one_edition_then_returns_only_tracks_for_active_edition()
    {
        $activeEdition = Edition::factory(['opened_at' => now(), 'closed_at' => null])->create();
        $nextEdition = Edition::factory(['opened_at' => null, 'closed_at' => null])->create();
        $oldEdition = Edition::factory([
            'opened_at' => now()->subYear(),
            'closed_at' => now()->subMonth()
        ])->create();

        $activeTrack = Track::factory()->for($activeEdition)->create();
        $oldTrack = Track::factory()->for($oldEdition)->create();
        $nextEdition = Track::factory()->for($nextEdition)->create();

        $this->currentEdition->method('find')->willReturn($activeEdition);
        $activeTracks = new ActiveTracks($this->currentEdition);
        $result = $activeTracks->find();

        $this->assertCount(1, $result);
    }
}
