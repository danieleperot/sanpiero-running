<?php

namespace Tests\OrmUnit;

use App\Models\Association;
use App\Models\Runner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssociationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_be_saved_to_db()
    {
        $association = new Association;
        $association->name = '::name::';

        $this->expectNotToPerformAssertions();
        $association->save();
    }

    /** @test */
    public function can_have_many_runners()
    {
        $association = Association::factory()->create();
        $otherAssociation = Association::factory()->create();

        $runner1 = Runner::factory()->for($association)->create();
        $runner2 = Runner::factory()->for($association)->create();
        Runner::factory()->for($otherAssociation)->create();

        $runners = $association->fresh()->runners->pluck('id')->toArray();
        $this->assertEquals([$runner1->id, $runner2->id], $runners);
    }

    /** @test */
    public function has_slug_computed_from_name()
    {
        $association = Association::factory(['name' => 'The name'])->create();

        $fresh = $association->fresh();
        $this->assertEquals('the-name', $fresh->slug);
    }

    /** @test */
    public function special_characters_are_not_considered_when_creating_slug()
    {
        $association1 = Association::factory(['name' => 'The name'])->make();
        $association2 = Association::factory(['name' => '  tHE .. = - name ? '])->make();

        $this->assertEquals($association1->slug, $association2->slug);
    }
}
