<?php

namespace Tests\OrmUnit;

use App\Actions\Dto\Runners\SaveNewPayload;
use App\Actions\Eloquent\Runners\RunnerCreator;
use App\Models\Association;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Assert;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class EloquentRunnerCreatorTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_instantiate_creator()
    {
        $creator = new RunnerCreator;
        $this->assertInstanceOf(\App\Actions\Api\Runners\RunnerCreator::class, $creator);
    }

    /** @test */
    public function creates_a_new_runner_with_a_new_person()
    {
        $track = Track::factory()->create();
        $payload = SaveNewPayload::from([
            'lastName' => 'Rossi',
            'firstName' => 'Mario',
            'birthdate' => '1990-08-15',
            'sex' => 'm',
            'number' => '1234',
            'trackId' => $track->id
        ]);

        $creator = new RunnerCreator;
        $creator->create($payload);

        $runner = Runner::whereNumber(1234)->first();
        $person = Person::whereLastName('Rossi')
            ->whereFirstName('Mario')
            ->whereSex('m')
            ->first();

        $this->assertEquals($payload->trackId, $runner->track->id);
        $this->assertEquals($person->id, $runner->person->id);
        $this->assertEquals('1990-08-15', $person->birthdate->format('Y-m-d'));
    }

    /** @test */
    public function creates_a_runner_with_a_new_association_if_association_name_is_provided()
    {
        $track = Track::factory()->create();
        $payload = SaveNewPayload::from([
            'lastName' => 'Rossi',
            'firstName' => 'Mario',
            'birthdate' => '1990-08-15',
            'sex' => 'm',
            'number' => '1234',
            'trackId' => $track->id,
            'association' => '::association::'
        ]);

        $creator = new RunnerCreator;
        $creator->create($payload);

        $runner = Runner::whereNumber(1234)->first();
        $association = Association::whereName('::association::')->first();
        $this->assertNotNull($association);
        $this->assertEquals($association->id, $runner->association->id);
    }

    /** @test */
    public function if_association_already_exists_for_that_name_then_that_one_is_used()
    {
        $association = Association::factory(['name' => '::association::'])->create();
        $track = Track::factory()->create();
        $payload = SaveNewPayload::from([
            'lastName' => 'Rossi',
            'firstName' => 'Mario',
            'birthdate' => '1990-08-15',
            'sex' => 'm',
            'number' => '1234',
            'trackId' => $track->id,
            'association' => '::association::'
        ]);

        $creator = new RunnerCreator;
        $creator->create($payload);

        $runner = Runner::whereNumber(1234)->first();
        $this->assertEquals(1, Association::whereName('::association::')->count());
        $this->assertEquals($association->id, $runner->association->id);
    }

    /** @test */
    public function association_finder_is_case_insensitive()
    {
        $association = Association::factory(['name' => '::association::'])->create();
        $track = Track::factory()->create();
        $payload = SaveNewPayload::from([
            'lastName' => 'Rossi',
            'firstName' => 'Mario',
            'birthdate' => '1990-08-15',
            'sex' => 'm',
            'number' => '1234',
            'trackId' => $track->id,
            'association' => '::ASSOciation::'
        ]);

        $creator = new RunnerCreator;
        $creator->create($payload);

        $runner = Runner::whereNumber(1234)->first();
        $this->assertEquals($association->id, $runner->association->id);
    }

    /** @test */
    public function association_finder_ignores_special_characters()
    {
        $association = Association::factory(['name' => 'A.R.C. Salzan'])->create();
        $track = Track::factory()->create();
        $payload = SaveNewPayload::from([
            'lastName' => 'Rossi',
            'firstName' => 'Mario',
            'birthdate' => '1990-08-15',
            'sex' => 'm',
            'number' => '1234',
            'trackId' => $track->id,
            'association' => 'arc salzan'
        ]);

        $creator = new RunnerCreator;
        $creator->create($payload);

        $runner = Runner::whereNumber(1234)->first();
        $this->assertEquals($association->id, $runner->association->id);
    }

    /**
     * @test
     * @dataProvider validationDataProvider
     */
    public function validates_input($payload = [], $expectedErrors = [], ?callable $callback = null)
    {
        if ($callback) call_user_func($callback);

        $savePayload = SaveNewPayload::from($payload);
        $creator = new RunnerCreator;

        try {
            $creator->create($savePayload);
            Assert::fail('No validation error encountered, but was expected');
        } catch (ValidationException $exception) {
            $errors = collect($exception->errors());
            $this->assertNotEmpty($errors->only($expectedErrors), 'Not all the expected errors were thrown');
        }
    }

    public function validationDataProvider()
    {
        return [
            'First name is required' => [[], ['firstName']],
            'Last name is required' => [[], ['lastName']],
            'Birthdate is required' => [[], ['birthdate']],
            'Sex is required' => [[], ['sex']],
            'Track Id is required' => [[], ['trackId']],
            'Track Id must be an existing track' => [['trackId' => 100], ['trackId']],
            'Number is required' => [[], ['number']],
            'Number cannot already exist for same track' => [
                ['trackId' => 100, 'number' => 5],
                ['number'],
                function () {
                    $track = Track::factory(['id' => 100])->create();
                    Runner::factory(['number' => 5])->for($track)->create();
                }
            ]
        ];
    }
}
