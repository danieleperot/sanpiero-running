<?php

namespace Tests\OrmUnit;

use App\Actions\Api\Editions\CurrentEdition;
use App\Actions\Dto\Runners\ListPayload;
use App\Actions\Eloquent\Runners\RunnersList;
use App\Models\Edition;
use App\Models\Person;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class EloquentRunnersListTest extends TestCase
{
    use RefreshDatabase;

    /** @var MockObject|CurrentEdition */
    private $currentEdition;

    protected function setUp(): void
    {
        parent::setUp();

        $this->currentEdition = $this->getMockBuilder(CurrentEdition::class)->getMock();
    }

    /** @test */
    public function returns_runners_with_person_information()
    {
        $person = Person::factory(['first_name' => '::name::'])->create();
        Runner::factory()->for($person)->create();

        $runnersList = (new RunnersList($this->currentEdition))->get(new ListPayload);

        $this->assertEquals('::name::', $runnersList->first()->person->first_name);
    }

    /** @test */
    public function returns_runners_with_track_information()
    {
        $track = Track::factory(['name' => '::track::'])->create();
        Runner::factory()->for($track)->create();

        $runnersList = (new RunnersList($this->currentEdition))->get(new ListPayload);

        $this->assertEquals('::track::', $runnersList->first()->track->name);
    }

    /** @test */
    public function provided_dto_determinates_current_page()
    {
        Runner::factory()->count(13)->create();

        $request = new ListPayload;
        $request->page = 2;
        $runnersList = (new RunnersList($this->currentEdition))->get($request);

        $this->assertEquals(2, $runnersList->currentPage());
    }

    /** @test */
    public function orders_by_desc_created_at_by_default()
    {
        $r2 = Runner::factory(['created_at' => now()->subDays(5)])->create();
        $r4 = Runner::factory(['created_at' => now()->subDays(15)])->create();
        $r1 = Runner::factory(['created_at' => now()->subDays(2)])->create();
        $r3 = Runner::factory(['created_at' => now()->subDays(10)])->create();

        $runnersList = (new RunnersList($this->currentEdition))->get(new ListPayload);

        $expected = [$r1->id, $r2->id, $r3->id, $r4->id];
        $this->assertEquals($expected, $runnersList->pluck('id')->toArray());
    }

    /** @test */
    public function can_list_runners_only_for_current_edition()
    {
        $currentEdition = Edition::factory()->create();
        $otherEdition = Edition::factory()->create();
        $track1 = Track::factory()->for($currentEdition)->create();
        $track2 = Track::factory()->for($otherEdition)->create();
        $runner1 = Runner::factory()->for($track1)->create();
        $runner2 = Runner::factory()->for($track2)->create();


        $payload = new ListPayload;
        $payload->editions = ListPayload::CURRENT_EDITON;

        $this->currentEdition->method('find')->willReturn($currentEdition);
        $runnersList = (new RunnersList($this->currentEdition))->get($payload);

        $this->assertCount(1, $runnersList);
    }
}
