<?php

namespace Tests\OrmUnit;

use App\Actions\Api\Editions\CurrentEdition as CurrentEditionInterface;
use App\Actions\Eloquent\Editions\CurrentEdition;
use App\Models\Edition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EloquentCurrentEditionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function returns_new_unsaved_edition_by_default()
    {
        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertInstanceOf(CurrentEditionInterface::class, $currentEdition);
        $this->assertFalse($result->exists);
    }

    /** @test */
    public function if_edition_exists_and_is_not_closed_then_its_returned()
    {
        $edition = Edition::factory()->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertEquals($edition->id, $result->id);
    }

    /** @test */
    public function if_edition_is_closed_in_the_past_then_its_not_saved()
    {
        $edition = Edition::factory(['closed_at' => now()->subDay()])->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertNotEquals($edition->id, $result->id);
    }

    /** @test */
    public function if_edition_close_date_is_in_the_future_then_it_is_returned()
    {
        $edition = Edition::factory(['closed_at' => now()->addDay()])->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertEquals($edition->id, $result->id);
    }

    /** @test */
    public function if_edition_has_open_date_and_is_in_the_past_then_it_is_returned()
    {
        $edition = Edition::factory(['opened_at' => now()->subDay()])->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertEquals($edition->id, $result->id);
    }

    /** @test */
    public function if_edition_has_open_date_and_is_in_the_future_then_it_is_not_returned()
    {
        $edition = Edition::factory(['opened_at' => now()->addDay()])->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertNotEquals($edition->id, $result->id);
    }

    /** @test */
    public function works_with_multiple_editions()
    {
        $activeEdition = Edition::factory(['opened_at' => now(), 'closed_at' => null])->create();
        $nextEdition = Edition::factory(['opened_at' => null, 'closed_at' => null])->create();
        $oldEdition = Edition::factory([
            'opened_at' => now()->subYear(),
            'closed_at' => now()->subMonth()
        ])->create();

        $currentEdition = new CurrentEdition;
        $result = $currentEdition->find();

        $this->assertNotEquals($activeEdition->id, $result->id);
    }
}
