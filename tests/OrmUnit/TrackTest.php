<?php

namespace Tests\OrmUnit;

use App\Models\Edition;
use App\Models\Runner;
use App\Models\Track;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TrackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function track_exists()
    {
        Track::factory()->create();

        $this->assertCount(1, Track::all());
    }

     /** @test */
     public function track_belongs_to_edition()
     {
         $edition = Edition::factory()->create();
         $track = Track::factory()->make();

         $track->edition()->associate($edition);
         $track->save();

         $this->assertEquals($edition->id, $track->edition_id);
     }

     /** @test */
     public function track_has_many_runners()
     {
         $track = Track::factory()->create();
         $otherTrack = Track::factory()->create();

         Runner::factory()->count(3)->for($track)->create();
         Runner::factory()->count(2)->for($otherTrack)->create();

         $this->assertEquals(3, $track->fresh()->runners()->count());
         $this->assertEquals(2, $otherTrack->fresh()->runners()->count());
     }
}
