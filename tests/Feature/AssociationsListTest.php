<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssociationsListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function not_logged_user_cannot_list_associations()
    {
        $this->get(route('associations.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_see_list_of_associations()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('associations.index'))
            ->assertSuccessful()
            ->assertViewIs('settings.associations');
    }
}
