<?php

namespace Tests\Feature;

use App\Models\Track;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TrackShowTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guest_cannot_access_track_details()
    {
        $track = Track::factory()->create();

        $this->get(route('tracks.show', $track))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_access_tracks_list()
    {
        $user = User::factory()->create();
        $track = Track::factory()->create();

        $this->actingAs($user)->get(route('tracks.show', $track))
            ->assertSuccessful()
            ->assertViewIs('tracks.show');
    }
}
