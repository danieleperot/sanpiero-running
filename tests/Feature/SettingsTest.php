<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SettingsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function not_logged_user_cannot_access_settings()
    {
        $this->get(route('settings.index'))->assertRedirect('/login');
    }

    /** @test */
    public function user_can_see_settings_page()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('settings.index'))
            ->assertRedirect(route('editions.index'));
    }
}
