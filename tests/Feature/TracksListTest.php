<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TracksListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guest_cannot_access_tracks_list()
    {
        $this->get(route('tracks.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_access_tracks_list()
    {
        $user = User::factory()->create();

        $this->actingAs($user)->get(route('tracks.index'))
            ->assertSuccessful()
            ->assertViewIs('tracks.index');
    }
}
