<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EditionsListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function not_logged_user_cannot_list_editions()
    {
        $this->get(route('editions.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_see_list_of_editions()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('editions.index'))
            ->assertSuccessful()
            ->assertViewIs('settings.editions.index');
    }
}
