<?php

namespace Tests\Feature;

use App\Models\Runner;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RunnersListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guest_cannot_visit_runners_list()
    {
        $this->get(route('runners.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function logged_user_sees_paginated_list_of_runners()
    {
        $user = User::factory()->create();
        $runners = Runner::factory()->count(5)->create();

        $response = $this->actingAs($user)->get(route('runners.index'));

        $response->assertSuccessful();
        $response->assertViewIs('runners.index');
        $response->assertSee($runners->first()->person->first_name);
    }
}
