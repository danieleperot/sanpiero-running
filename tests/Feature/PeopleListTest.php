<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PeopleListTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function not_logged_user_cannot_list_people()
    {
        $this->get(route('people.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_see_list_of_people()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('people.index'))
            ->assertSuccessful()
            ->assertViewIs('settings.people');
    }
}
