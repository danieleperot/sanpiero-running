<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function registration_view_should_not_be_available()
    {
        $this->get('/register')->assertNotFound();
    }

    /** @test */
    public function registration_route_should_not_be_available()
    {
        $this->post('/register')->assertNotFound();
    }
}
