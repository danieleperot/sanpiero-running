<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReportsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function not_logged_user_cannot_view_reports()
    {
        $this->get(route('reports.index'))->assertRedirect(route('login'));
    }

    /** @test */
    public function user_can_see_reports()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('reports.index'))
            ->assertSuccessful()
            ->assertViewIs('settings.reports');
    }
}
