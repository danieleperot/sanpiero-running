<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Mix;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();
        $app->bind(Mix::class, MixTest::class);

        return $app;
    }
}

class MixTest extends Mix
{
    public function __invoke($path, $manifestDirectory = '')
    {
        return $path;
    }
}
